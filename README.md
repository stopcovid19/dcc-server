# DCC Server

Ce projet gitlab.inria.fr est un des composants de la solution plus globale [TousAntiCovid](https://gitlab.inria.fr/stopcovid19/accueil).

Ce composant est responsable de la génération de pass light, dans certains cas spécifiques.

## Contrat d'interface

Le contrat d'interface du service REST exposé est disponible au format OpenAPI [openapi-dcclight.yaml](src/main/resources/openapi-dcclight-v1.yml)

## Configuration

### Clés de signature

L'application utilise une clé de signature générée à l'aide de commandes openssl trouvables dans le fichier `generate_signing_key.sh` à la racine du projet.  
Il existe une clé `dcclight` (signée par l'INRIA, portée nationale) et une clé `dcc` (signée par IN groupe, portée européenne).

| Clé de configuration                    | Description                                                          |
| --------------------------------------- | -------------------------------------------------------------------- |
| `dcclight.encryption-private-key`       | Clé privée à utiliser pour déchiffrer le contenu des requêtes        |
| `dcclight.dcc.signing-private-key`      | Clé privée à utiliser pour la production de DCC                      |
| `dcclight.dcc.signing-certificate`      | Clé publique à utiliser pour la production de DCC[^1]                |
| `dcclight.dcclight.signing-private-key` | Clé privée à utiliser pour la production de _éphémère_               |
| `dcclight.dcclight.signing-certificate` | Clé publique à utiliser pour la production de DCC _DCC éphémère_[^1] |

[^1]: les clés publiques ne servent pas à vérifier les DCC reçus en entrée de l'API, elles sont utilisées par la [lib hcert](https://github.com/ehn-dcc-development/hcert-java) pour la production des DCC. La vérification de la signature des DCC reçus en entrée se fait avec les certificats récupérés via le paramètre `dcclight.signing-certificates-endpoint.url`.

### Configuration externalisée

Certains éléments de configuration sont externalisés et maintenus à jour à intervalle régulier.

| Paramètre                       | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| [...]`.url`                     | URL à laquelle récupérer le fichier de configuration externe               |
| [...]`.refresh-scheduling.cron` | Fréquence à laquelle la configuration doit être mise à jour                |
| [...]`.token`                   | (optionnel) Ajoute un header `Authentication: Bearer {token}` à la requête |

| Clé de configuration                      | Description               |
| ----------------------------------------- | ------------------------- |
| `dcclight.aggregation-rules-endpoint.`    | Règles d'aggrégation      |
| `dcclight.blocklist-endpoint.`            | DCC révoqués              |
| `dcclight.signing-certificates-endpoint.` | Émetteurs de DCC acceptés |

#### Format configuration externe _Règles d'aggrégation_

Fichier au format Yaml:

```yaml
rules: # liste de règles d'aggrégation de preuves
  - uniqueID: string # identifiant unique de la règle, doit rester constant pour l'observation des métriques
    description: string # description de la règle, utilisé à titre de documentation dans les métriques
    isRuleActive: boolean
    input: # 1 élément par DCC attendu en entrée
      - type: enum # "t" pour "test", "v" pour vaccin, "r" pour rétablissement
        proofDateConstraints: # contraintes sur la date de la preuve
          validIfAfter: ISO 8601 Date # la preuve a été émise après la date donnée (ex: 2022-10-19)
          validIfBefore: ISO 8601 Date # la preuve a été émise avant la date donnée  (ex: 2022-10-19)
          validIfDatedWithin: ISO 8601 Duration # la preuve a été émise durant la période donnée (relative à l'instant courant) (ex: P10DT12H)
          minDaysAfterPreviousDcc: integer # nombre de jours minimum qui doivent séparer la date de la preuve courante et la précédente (ignoré pour le premier élément)
        allowLists: # liste de valeurs acceptées
          # bigramme de l'attribut: tableau de valeurs acceptées
          tg: ["valeur acceptée"]
        blockLists: # liste de valeurs exclues
          # bigramme de l'attribut: tableau de valeurs exclues
          tc: ["valeur interdite"]
    output:
      schema: enum # "DCC" ou "DCCLIGHT"
      referentialDccRanking: integer # index du DCC de référence (commençant à 1), celui dont les propriétés serviront de base pour la construction du DCC à générer
      type: enum # "t" pour "test", "v" pour vaccin, "r" pour rétablissement
      overrideValues: # valeurs d'attributs à définir dans le DCC cible
        # bigramme de l'attribut: valeur à mettre dans le DCC cible
        ex: "valeur" # définit la valeur 'valeur' dans l'attribut 'is' du DCC cible
      copyValues: # valeurs d'attributs à copier à partir du DCC de référence
        # bigramme dans le DCC cible: bigramme de l'attribut du DCC de référence à copier
        ex: "sr" # copie la valeur de l'attribut 'sr' du DCC de référence dans l'attribut 'ex' du DCC cible
      incDateByDays: # valeurs d'attributs de type date à incrémenter dans le DCC cible (aucun effet si l'attribut n'est pas une date ou une datetime)
        # bigramme dans le DCC cible: nombre de jours à ajouter
        ex: 11 # incrémente l'attribut 'ex' de 11 jours
```

Le fichier est au format Yaml et les spécificités de ce format peuvent être utilisées dans la limite de ce qui est supporté par la librairie [SnakeYaml](https://github.com/snakeyaml/snakeyaml).

#### Format configuration externe _UVCI révoqués_

Fichier au format JSON:

```json
[
  "8459fa844532dd0dfc38fc8561f363296bd736a782be8afdf1c8998c64dabece",
  "Encoded DCC identifier",
  "..."
]
```

#### Format configuration externe _Émetteurs de DCC acceptés_

Fichier au format JSON:

```json
{
  "5UYTharhugo=": [
    "Ex8PNV0VQBuhu71PAag8nuKl0GlLKZkHZuct1ZY7XQ2ERTol2EQaBS/UKe7VN9ArP4epFutqNzAwUJnQRdCap4QjQ0z/Cg=="
  ],
  "kid": ["public key"],
  "...": ["..."]
}
```
