FROM eclipse-temurin:11-jre-alpine

RUN adduser -S -u 101 appuser

COPY ./target/dcclight-server-*.jar dcclight-server.jar

USER appuser

CMD ["java", "-jar", "dcclight-server.jar"]

EXPOSE 8080 8081
