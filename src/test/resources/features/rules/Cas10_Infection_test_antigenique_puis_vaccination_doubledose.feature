# language: fr
Fonctionnalité: Cas10 Infection (test) puis vaccination doubledose

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 2/2 et preuve de test antigénique positive
    Etant donné une preuve positive de test antigénique 1341 effectué il y a 105 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin> 2/2 injecté il y a 10 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 2/1

    Exemples:
      | vaccin               |
      | Pfizer               |
      | Moderna              |
      | Astra-zeneca         |
      | Covid-19-recombinant |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve positive de test antigénique 1341 effectué il y a 105 jours en <code pays 1> par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 5 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par DGSAG
    Et la preuve de vaccination a un nombre de doses total de 2/1

    Exemples:
      | code pays 1   | code pays 2 |
      | FR            | FR          |
      | FR            | DE          |
      | FR            | ES          |
      | FR            | IT          |
      | FR            | LU          |
      | FR            | BE          |
      | FR            | CH          |
      | FR            | DK          |

  Scénario: Cas de rejet si test PCR et vaccin 2/2 valide
    Etant donné une preuve négative de test pcr sans fabricant effectué il y a 100 jours en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 5 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si test antigénique avec date de validité passée et vaccin 2/2 valide
    Etant donné une preuve positive de test antigénique 1341 effectué il y a 3800 jours en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 5 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si test antigénique valide réalisé en Allemagne et vaccin 2/2 valide
    Etant donné une preuve positive de test antigénique 1341 effectué il y a 100 jours en DE par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 5 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de vaccin Pfizer <doses vaccin> injecté il y a 5 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve positive de test antigénique 1341 effectué il y a 100 jours en FR par CNAM au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin |
      | 1/2          |
      | 2/1          |
      | 3/2          |
      | 3/3          |

  Scénario: Cas de rejet si le manufacturer n'est pas dans l'allowlist
    Etant donné une preuve positive de test antigénique 0000 effectué il y a 105 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 10 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve
