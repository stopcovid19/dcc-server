# language: fr
Fonctionnalité: Cas9bis Vaccination 1/1 puis infection (PCR+)

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 1/1 et preuve de test pcr positive
    Etant donné une preuve de vaccin <vaccin> 1/1 injecté il y a 95 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve positive de test pcr 1341 effectué il y a 2 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type test valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de test est émise par DGSAG
    Et la preuve de test a été réalisée dans le centre TV

    Exemples:
      | vaccin               |
      | Pfizer               |
      | Moderna              |
      | Astra-zeneca         |
      | Novavax              |
      | Covid-19-recombinant |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Pfizer 1/1 injecté il y a 95 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve positive de test pcr 1341 effectué il y a 2 jours en <code pays 1> par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type test valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de test est émise par DGSAG
    Et la preuve de test a été réalisée dans le centre TV

    Exemples:
      | code pays 2   | code pays 1 |
      | DE            | FR          |
      | FR            | DE          |
      | IT            | ES          |
      | FR            | IT          |
      | SP            | LU          |
      | LU            | BE          |
      | UK            | CH          |
      | FR            | DK          |
    
  Scénario: Cas de rejet si test pcr avec date de validité passée et vaccin 1/1 valide
    Etant donné une preuve positive de test pcr 1341 effectué il y a 400 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 95 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve positive de test pcr 1341 effectué il y a 2 jours en FR par CNAM au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin> injecté il y a 95 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin |
      | 2/1          |
      | 3/2          |
      | 3/3          |
      | 1/2          |
