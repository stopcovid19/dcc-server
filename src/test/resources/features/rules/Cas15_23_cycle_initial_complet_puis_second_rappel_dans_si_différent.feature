# language: fr
Fonctionnalité: Cycle initial complet et second rappel dans SI differents

  Plan du Scénario: (15) Cas nominaux avec différentes combinaisons de vaccins 2/2 et 2/2
    Etant donné une preuve de vaccin <vaccin 1> 2/2 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 2/2 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par TV-2I-DGSAG
    Et la preuve de vaccination a un nombre de doses total de 4/4

    Exemples:
      | vaccin 1              | vaccin 2     |
      | Pfizer                | Moderna      |
      | Moderna               | Astra-zeneca |
      | Astra-zeneca          | Astra-zeneca |
      | Novavax               | Pfizer       |
      | Pfizer                | Pfizer       |
      | Moderna               | Moderna      |
      | Astra-zeneca          | Astra-zeneca |
      | Covid-19-recombinant  | Moderna      |

  Plan du Scénario: (16) Cas nominaux avec différentes combinaisons de vaccins 2/2 et 1/1 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 2/2 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 3> 1/1 injecté il y a 15 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par TV-2I-DGSAG
    Et la preuve de vaccination a un nombre de doses total de 4/4

    Exemples:
      | vaccin 1              | vaccin 2     | vaccin 3     |
      | Pfizer                | Moderna      | Astra-zeneca |
      | Moderna               | Astra-zeneca | Moderna      |
      | Astra-zeneca          | Astra-zeneca | Pfizer       |
      | Novavax               | Pfizer       | Moderna      |
      | Pfizer                | Pfizer       | Pfizer       |
      | Moderna               | Moderna      | Moderna      |
      | Astra-zeneca          | Astra-zeneca | Astra-zeneca |
      | Covid-19-recombinant  | Moderna      | Moderna      |

  Plan du Scénario: (17) Cas nominaux avec différentes combinaisons de vaccins 1/1 et 1/1 et 1/1 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 1/1 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 3> 1/1 injecté il y a 15 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 4> 1/1 injecté il y a 5 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par TV-3I-DGSAG
    Et la preuve de vaccination a un nombre de doses total de 4/1

    Exemples:
      | vaccin 1              | vaccin 2     | vaccin 3     | vaccin 4     |
      | Pfizer                | Moderna      | Astra-zeneca | Astra-zeneca |
      | Moderna               | Astra-zeneca | Moderna      | Moderna      |
      | Astra-zeneca          | Astra-zeneca | Pfizer       | Pfizer       |
      | Novavax               | Pfizer       | Moderna      | Moderna      |
      | Pfizer                | Pfizer       | Pfizer       | Pfizer       |
      | Moderna               | Moderna      | Moderna      | Moderna      |
      | Astra-zeneca          | Astra-zeneca | Astra-zeneca | Astra-zeneca |
      | Covid-19-recombinant  | Moderna      | Moderna      | Moderna      |

  Plan du Scénario: (20) Cas nominaux avec différentes combinaisons de vaccins 3/3 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 3/3 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par TV-3I-DGSAG
    Et la preuve de vaccination a un nombre de doses total de 4/4

    Exemples:
      | vaccin 1              | vaccin 2     |
      | Pfizer                | Moderna      |
      | Moderna               | Astra-zeneca |
      | Astra-zeneca          | Astra-zeneca |
      | Novavax               | Pfizer       |
      | Pfizer                | Pfizer       |
      | Moderna               | Moderna      |
      | Astra-zeneca          | Astra-zeneca |
      | Covid-19-recombinant  | Moderna      |


  Plan du Scénario: (21) Cas nominaux avec différentes combinaisons de vaccins 2/1 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 2/1 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par TV-2I-DGSAG
    Et la preuve de vaccination a un nombre de doses total de 3/1

    Exemples:
      | vaccin 1              | vaccin 2     |
      | Pfizer                | Moderna      |
      | Moderna               | Astra-zeneca |
      | Astra-zeneca          | Astra-zeneca |
      | Novavax               | Pfizer       |
      | Pfizer                | Pfizer       |
      | Moderna               | Moderna      |
      | Astra-zeneca          | Astra-zeneca |
      | Covid-19-recombinant  | Moderna      |

  Plan du Scénario: (23) Cas nominaux avec différentes combinaisons de vaccins 2/2 oms et 2/2 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 2/2 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 2/2 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 3> 1/1 injecté il y a 15 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et la preuve de vaccination est émise par TV-2I-DGSAG
    Et la preuve de vaccination a un nombre de doses total de 4/4

    Exemples:
      | vaccin 1  | vaccin 2              | vaccin 3     |
      | Covovax   | Pfizer                | Moderna      |
      | Sinovac   | Moderna               | Astra-zeneca |
      | Bharat    | Astra-zeneca          | Astra-zeneca |
      | Covovax   | Moderna               | Pfizer       |
      | Bharat    | Pfizer                | Pfizer       |
      | Bharat    | Moderna               | Moderna      |
      | Sinovac   | Astra-zeneca          | Astra-zeneca |
      | Sinovac   | Covid-19-recombinant  | Moderna      |