# language: fr
Fonctionnalité: Cas1 cycle initial complet et rappel dans SI différents

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 2/2 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 2/2 injecté il y a 130 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 39 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979

    Exemples:
      | vaccin 1              | vaccin 2     |
      | Pfizer                | Moderna      |
      | Moderna               | Astra-zeneca |
      | Astra-zeneca          | Astra-zeneca |
      | Novavax               | Pfizer       |
      | Pfizer                | Pfizer       |
      | Moderna               | Moderna      |
      | Astra-zeneca          | Astra-zeneca |
      | Covid-19-recombinant  | Moderna      |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 130 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Moderna 1/1 injecté il y a 39 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979

    Exemples:
      | code pays 1   | code pays 2 |
      | FR            | FR          |
      | DE            | DE          |
      | ES            | ES          |
      | IT            | IT          |
      | LU            | LU          |
      | BE            | BE          |
      | CH            | CH          |
      | DK            | DK          |

  Scénario: Cas nominal si preuve 2/2 avec une date de validité passée et preuve 1/1 valide
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 400 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 30 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000

  Scénario: Cas de rejet si preuve 1/1 avec type de vaccin erroné et preuve 2/2 valide
    Etant donné une preuve de vaccin mauvais medicinal product 1/1 injecté il y a 130 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 2/2 injecté il y a 39 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Scénario: Cas de rejet si liste de preuves valides avec dans l'ordre "un vaccin 2/2, un vaccin 1/1, un vaccin 1/2"
    Etant donné une preuve de vaccin Pfizer 2/2 injecté il y a 130 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 39 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Et une preuve de vaccin Pfizer 1/2 injecté il y a 95 jours en FR au nom de MARIE, GLADYS<DADOU - Marie, Gladys-Dadou né le 29/02/2000
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de vaccin Pfizer <doses vaccin 1> injecté il y a 130 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Et une preuve de vaccin Moderna <doses vaccin 2> injecté il y a 39 jours en FR au nom de PIERRE, CURIE - Pierre, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin 1 | doses vaccin 2 |
      | 3/1            | 1/1            |
      | 1/2            | 1/1            |