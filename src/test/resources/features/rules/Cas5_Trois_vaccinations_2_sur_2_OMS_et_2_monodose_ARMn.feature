# language: fr
Fonctionnalité: Cas5 Trois vaccinations 2/2 OMS et 2 monodose ARMn

  Plan du Scénario: Cas nominaux avec différentes combinaisons de vaccins 2/2 et 1/1
    Etant donné une preuve de vaccin <vaccin 1> 2/2 injecté il y a 150 jours en IN au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 2> 1/1 injecté il y a 110 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin <vaccin 3> 1/1 injecté il y a 10 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979

    Exemples:
      | vaccin 1  | vaccin 2 | vaccin 3 |
      | Covovax   | Pfizer   | Moderna  |
      | Sinovac   | Moderna  | Pfizer   |
      | Bharat    | Pfizer   | Pfizer   |

  Plan du Scénario: Cas nominaux avec différentes combinaisons de pays et schéma vaccinal valide
    Etant donné une preuve de vaccin Covovax 2/2 injecté il y a 150 jours en <code pays 1> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer 1/1 injecté il y a 110 jours en <code pays 2> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Etant donné une preuve de vaccin Moderna 1/1 injecté il y a 10 jours en <code pays 3> au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors réception d'une preuve de type vaccination valide à partir de maintenant au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979

    Exemples:
      | code pays 1  | code pays 2 | code pays 3 |
      | IN           | FR          | DE          |
      | CN           | DE          | ES          |
      | GT           | ES          | FR          |
      | HK           | IT          | LU          |
      | IL           | LU          | BE          |
      | ID           | BE          | IT          |

  Plan du Scénario: Cas de rejets si le nombre de doses est incompatible
    Etant donné une preuve de vaccin Sinovac <doses vaccin 1> injecté il y a 150 jours en IN au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Pfizer <doses vaccin 2> injecté il y a 110 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Et une preuve de vaccin Moderna <doses vaccin 3> injecté il y a 10 jours en FR au nom de MARIE, CURIE - Marie, Curie né le 14/04/1979
    Quand une demande d'agrégation de preuves est réalisée
    Alors l'utilisateur ne reçoit pas de preuve

    Exemples:
      | doses vaccin 1 | doses vaccin 2 | doses vaccin 3 |
      | 3/1            | 1/1            | 1/1            |
      | 1/1            | 1/1            | 1/1            |
      | 2/1            | 1/1            | 1/1            |
      | 3/3            | 1/1            | 1/1            |
