package fr.gouv.tac.dcclight.extension

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Test

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores::class)
class CollectionTest {

    @Test
    fun empty_set_returns_true() {
        assertTrue(setOf<String>().isEmptyOrContains("whatever"))
    }

    @Test
    fun empty_set_returns_false_when_element_is_absent() {
        assertFalse(setOf("chose").isEmptyOrContains("whatever"))
    }

    @Test
    fun set_containing_element_returns_true_when_element_checked() {
        assertTrue(setOf("whatever").isEmptyOrContains("whatever"))
    }
}
