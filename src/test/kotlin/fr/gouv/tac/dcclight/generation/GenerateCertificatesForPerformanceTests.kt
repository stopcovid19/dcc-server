package fr.gouv.tac.dcclight.generation

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import ehn.techiop.hcert.kotlin.data.Person
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.DccSigningKeys
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.TEST_TRANSPORT_KEYS
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.TransportKeys
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withDateOfBirth
import fr.gouv.tac.dcclight.test.extension.withFamilyName
import fr.gouv.tac.dcclight.test.extension.withFamilyNameTransliterated
import fr.gouv.tac.dcclight.test.extension.withGivenName
import fr.gouv.tac.dcclight.test.extension.withGivenNameTransliterated
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.matchesUvciPattern
import fr.gouv.tac.dcclight.test.matchers.where
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.LocalDate
import kotlinx.datetime.toKotlinLocalDate
import org.apache.commons.lang3.RandomStringUtils
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus.OK
import java.nio.file.Path
import java.time.Duration
import java.time.ZonedDateTime
import java.util.Locale
import java.util.concurrent.ThreadLocalRandom
import kotlin.io.path.outputStream
import kotlin.math.absoluteValue
import kotlin.random.Random
import kotlin.time.Duration.Companion.days

@IntegrationTest
class GenerateCertificatesForPerformanceTests() {

    // keys used to encrypt certificates in request body
    private val transportKeys: TransportKeys = TEST_TRANSPORT_KEYS

    // keys used to sign DCCs included in request body
    private val signingKeys: DccSigningKeys = DCC_TEST_INPUT_KEYS

    /**
     * Generate random date between "now minus 18 years" and "now minus 100 years"
     */
    private fun getRandomDate(): LocalDate {
        val dayCountIn100Years = Duration.between(
            ZonedDateTime.now().minusYears(18),
            ZonedDateTime.now().minusYears(100)
        ).toDays().absoluteValue
        val random = ThreadLocalRandom.current().nextLong(dayCountIn100Years)
        return ZonedDateTime.now()
            .minusYears(18L + random)
            .toLocalDate()
            .toKotlinLocalDate()
    }

    private fun getRandomName(): String {
        return RandomStringUtils.randomAlphanumeric(Random.nextInt(5, 20))
    }

    @Disabled("skip load test requests generation")
    @Test
    fun generate_certificate_vaccination() {
        val path = Path.of("/tmp/requests-A")
        println("writing requests to $path")
        for (i in 1..100000) {
            path.resolve("request-$i.json")
                .outputStream()
                .bufferedWriter()
                .use { it.write(createRequestBody(transportKeys, signingKeys)) }
        }
    }

    @Test
    fun createRequestBodyInFile_generates_request_with_dcc_encoded_and_encrypted() {
        givenBaseHeaders()
            .body(
                createRequestBody(TEST_TRANSPORT_KEYS, DCC_TEST_INPUT_KEYS)
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .withProofType("v")
                                .where("dn", 3)
                                .where("ma", "ORG-100030215")
                                .where("vp", "1119349007")
                                .where("dt", today(minus = 39.days).toString())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("mp", "EU/1/20/1528")
                                .where("is", "CNAM")
                                .where("sd", 3)
                                .where("tg", "840539006")
                        )
                    )
            )
    }

    private fun createRequestBody(transportKeys: TransportKeys, dccSigningKeys: DccSigningKeys): String {
        val greenCertificates: List<GreenCertificate> = generate_vaccines()

        val dccAgregationRequest = DccAggregationRequest(
            publicKey = CLIENT_PUBLIC_KEY_BASE64,
            certificates = greenCertificates
                .map { encodeToDccAndEncrypt(it, transportKeys = transportKeys, dccSigningKeys = dccSigningKeys) }
        )

        return jacksonObjectMapper()
            .writeValueAsString(dccAgregationRequest)
    }

    private fun generate_vaccines(): List<GreenCertificate> {
        val familyName = getRandomName()
        val name = getRandomName()
        val person = Person(
            familyName = familyName,
            familyNameTransliterated = familyName.uppercase(Locale.getDefault()),
            givenName = name,
            givenNameTransliterated = name.uppercase(Locale.getDefault())
        )

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))
            .withGivenName(person.givenName)
            .withGivenNameTransliterated(person.givenNameTransliterated)
            .withFamilyName(person.familyName)
            .withFamilyNameTransliterated(person.familyNameTransliterated)
            .withDateOfBirth(getRandomDate())

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 39.days))
            .withGivenName(person.givenName)
            .withGivenNameTransliterated(person.givenNameTransliterated)
            .withFamilyName(person.familyName)
            .withFamilyNameTransliterated(person.familyNameTransliterated)
            .withDateOfBirth(vaccinationDcc.dateOfBirth!!)

        return listOf(
            vaccinationDcc,
            vaccination2Dcc
        )
    }
}
