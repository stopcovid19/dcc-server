package fr.gouv.tac.dcclight.e2e

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getProofInstant
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.DccSigningKeys
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.TransportKeys
import fr.gouv.tac.dcclight.test.eu.DccCryptographer
import fr.gouv.tac.dcclight.test.extension.base64Encode
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

interface Certificate {
    fun encrypt(transportKeys: TransportKeys): String
}

data class RawCertificate(val rawDcc: String) : Certificate {
    override fun encrypt(transportKeys: TransportKeys) =
        TransportEncryptionKeysManager.encryptForServer(rawDcc.toByteArray(), transportKeys)
            .base64Encode()
}

data class ModifiableCertificate(
    val greenCertificate: GreenCertificate,
    val signingKeys: DccSigningKeys,
    val issuedAt: Instant = greenCertificate.getProofInstant()
) : Certificate {

    override fun encrypt(transportKeys: TransportKeys) = DccCryptographer.encodeToDccAndEncrypt(
        greenCertificate,
        FixedClock(issuedAt),
        transportKeys = transportKeys,
        dccSigningKeys = signingKeys
    )

    private class FixedClock(private val instant: Instant) : Clock {
        override fun now() = instant
    }
}
