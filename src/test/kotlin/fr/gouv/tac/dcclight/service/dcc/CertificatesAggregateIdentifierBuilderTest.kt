package fr.gouv.tac.dcclight.service.dcc

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.matchesRegex
import org.junit.jupiter.api.Test

class CertificatesAggregateIdentifierBuilderTest {

    @Test
    fun non_regression_test_on_identifier_builder() {
        val identifierBuilder = CertificatesAggregateIdentifierBuilder(
            listOf(
                "AZ12584FHAZSSKJHFZECKLHAZDMLHAECMJLZHEFKJH",
                "AZ12584F42AZSSKJHFZEC66HAZDMLHAECMJLZ5EFKJ"
            )
        )

        assertThat(identifierBuilder.build(), equalTo("URN:UVCI:01:FR:DGSAG/3OYBXUBEY2X4ZFYD877II36NQC2XOVKJ9XU9KQFQDVRAQU4K6#8"))
    }

    @Test
    fun generates_dcc_matching_with_different_lengths_of_inputs() {
        val identifierBuilder = CertificatesAggregateIdentifierBuilder(
            listOf(
                "",
                "input hasardeuse qu'on va voir si ça fonctionne",
                "attention les yeux ça fonctionne",
            )
        )

        assertThat(identifierBuilder.build(), matchesRegex("^URN:UVCI:01:FR:DGSAG/[A-Z0-9]{49}#[A-Z0-9:/]$"))
    }
}
