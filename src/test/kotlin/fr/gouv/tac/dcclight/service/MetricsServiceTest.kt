package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.chain.Error
import fr.gouv.tac.dcclight.service.error.DccValidationError
import fr.gouv.tac.dcclight.test.IntegrationTest
import io.micrometer.core.instrument.MeterRegistry
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.tuple
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

@IntegrationTest
class MetricsServiceTest(@Autowired private val meterRegistry: MeterRegistry) {

    @Test
    fun one_metric_per_source_and_per_functional_error_exists() {
        val expectedFunctionErrorCounters = listOf("PASSPLUSCOVID_V1", "DEFAULT").flatMap { requestSource ->
            DccValidationError.values().map { error ->
                tuple("dcc.request.aggregation.error", requestSource, error.name)
            }
        }

        assertThat(meterRegistry.meters)
            .extracting({ it.id.name }, { it.id.getTag("source") }, { it.id.getTag("type") })
            .containsAll(expectedFunctionErrorCounters)
            .hasSizeGreaterThan(0)
    }

    @Test
    fun one_metric_per_source_and_per_hcert_error_exists() {
        val expectedHcertErrorCounters = listOf("PASSPLUSCOVID_V1", "DEFAULT").flatMap { requestSource ->
            Error.values().map { error ->
                tuple("dcc.request.hcert.error", requestSource, error.name)
            }
        }

        assertThat(meterRegistry.meters)
            .extracting({ it.id.name }, { it.id.getTag("source") }, { it.id.getTag("type") })
            .containsAll(expectedHcertErrorCounters)
            .hasSizeGreaterThan(0)
    }
}
