package fr.gouv.tac.dcclight.rules.model

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenExposedRules
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.antigenicTestYamlInputRule
import fr.gouv.tac.dcclight.test.defaultEmptyRuleList
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.addInputRule
import fr.gouv.tac.dcclight.test.extension.withOutputRule
import fr.gouv.tac.dcclight.test.extension.withRecoveryCountry
import fr.gouv.tac.dcclight.test.extension.withRecoveryDateOfFirstPositiveTestResult
import fr.gouv.tac.dcclight.test.extension.withRecoveryIdentifier
import fr.gouv.tac.dcclight.test.extension.withRecoveryIssuer
import fr.gouv.tac.dcclight.test.extension.withRecoveryTarget
import fr.gouv.tac.dcclight.test.extension.withRecoveryValidFrom
import fr.gouv.tac.dcclight.test.extension.withRecoveryValidUntil
import fr.gouv.tac.dcclight.test.extension.withTestCountry
import fr.gouv.tac.dcclight.test.extension.withTestDateTimeResult
import fr.gouv.tac.dcclight.test.extension.withTestDateTimeSample
import fr.gouv.tac.dcclight.test.extension.withTestFacility
import fr.gouv.tac.dcclight.test.extension.withTestIdentifier
import fr.gouv.tac.dcclight.test.extension.withTestIssuer
import fr.gouv.tac.dcclight.test.extension.withTestNameNaa
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withTestResult
import fr.gouv.tac.dcclight.test.extension.withTestTarget
import fr.gouv.tac.dcclight.test.extension.withTestType
import fr.gouv.tac.dcclight.test.extension.withVaccinationCountry
import fr.gouv.tac.dcclight.test.extension.withVaccinationDate
import fr.gouv.tac.dcclight.test.extension.withVaccinationDoseNumber
import fr.gouv.tac.dcclight.test.extension.withVaccinationDoseTotalNumber
import fr.gouv.tac.dcclight.test.extension.withVaccinationIdentifier
import fr.gouv.tac.dcclight.test.extension.withVaccinationIssuer
import fr.gouv.tac.dcclight.test.extension.withVaccinationManufacturer
import fr.gouv.tac.dcclight.test.extension.withVaccinationMedicinalProduct
import fr.gouv.tac.dcclight.test.extension.withVaccinationTarget
import fr.gouv.tac.dcclight.test.extension.withVaccinationVaccine
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationInputRule
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.recoveryInputRule
import fr.gouv.tac.dcclight.test.recoveryStatementCertificate
import fr.gouv.tac.dcclight.test.recoveryToTestOutputRule
import fr.gouv.tac.dcclight.test.testOutputRule
import fr.gouv.tac.dcclight.test.today
import fr.gouv.tac.dcclight.test.vaccine2_2OutputRule
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days

@TestInstance(PER_CLASS)
@IntegrationTest
class RulesInputBlockListTest {

    private fun getVaccine1_1ToVaccine2_2BlockListRule(): YamlRules {
        val vaccineInputRule = pfizer1_1VaccinationInputRule().copy(
            blockLists = mapOf(
                "tg" to listOf("IN_BLOCK_LIST"),
                "vp" to listOf("IN_BLOCK_LIST"),
                "mp" to listOf("IN_BLOCK_LIST"),
                "ma" to listOf("IN_BLOCK_LIST"),
                "is" to listOf("IN_BLOCK_LIST"),
                "ci" to listOf("IN_BLOCK_LIST"),
                "co" to listOf("IN_BLOCK_LIST"),
                "dt" to listOf("1998-07-12")
            )
        )
        return defaultEmptyRuleList()
            .addInputRule(vaccineInputRule)
            .withOutputRule(vaccine2_2OutputRule().copy(referentialDccRanking = 1))
    }

    private fun getTestToTestBlockListRule(): YamlRules {
        val testInputRule = antigenicTestYamlInputRule().copy(
            blockLists = mapOf(
                "tg" to listOf("IN_BLOCK_LIST"),
                "tt" to listOf("IN_BLOCK_LIST"),
                "nm" to listOf("IN_BLOCK_LIST"),
                "ma" to listOf("IN_BLOCK_LIST"),
                "sc" to listOf("1850-01-01T10:10:10Z"),
                "dr" to listOf("1750-01-01T10:10:10Z"),
                "tr" to listOf("IN_BLOCK_LIST"),
                "tc" to listOf("IN_BLOCK_LIST"),
                "is" to listOf("IN_BLOCK_LIST"),
                "ci" to listOf("IN_BLOCK_LIST"),
                "co" to listOf("IN_BLOCK_LIST")
            )
        )
        return defaultEmptyRuleList()
            .addInputRule(testInputRule)
            .withOutputRule(testOutputRule())
    }

    private fun getRecoveryToTestBlockListRule(): YamlRules {
        val inputRule = recoveryInputRule().copy(
            blockLists = mapOf(
                "tg" to listOf("IN_BLOCK_LIST"),
                "fr" to listOf("1650-01-01"),
                "co" to listOf("IN_BLOCK_LIST"),
                "is" to listOf("IN_BLOCK_LIST"),
                "df" to listOf("1850-01-01"),
                "du" to listOf("1750-01-01"),
                "ci" to listOf("IN_BLOCK_LIST")
            )
        )
        return defaultEmptyRuleList()
            .addInputRule(inputRule)
            .withOutputRule(recoveryToTestOutputRule())
    }

    @Test
    fun vaccine_with_no_attribute_in_rule_input_block_lists_must_return_a_new_certificate() {
        givenExposedRules(getVaccine1_1ToVaccine2_2BlockListRule())

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 50.days))
        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(encodeToDccAndEncrypt(vaccinationDcc)),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
    }

    private fun vaccinationProofs(): List<Arguments> {
        val vaccination2Dcc = pfizer2_2VaccinationCertificate(today(minus = 140.days))
            .withVaccinationDoseTotalNumber(3)
            .withVaccinationDoseNumber(3)

        return listOf(
            Arguments.of("tg", vaccination2Dcc.withVaccinationTarget("IN_BLOCK_LIST")),
            Arguments.of("vp", vaccination2Dcc.withVaccinationVaccine("IN_BLOCK_LIST")),
            Arguments.of("mp", vaccination2Dcc.withVaccinationMedicinalProduct("IN_BLOCK_LIST")),
            Arguments.of("ma", vaccination2Dcc.withVaccinationManufacturer("IN_BLOCK_LIST")),
            Arguments.of("is", vaccination2Dcc.withVaccinationIssuer("IN_BLOCK_LIST")),
            Arguments.of("ci", vaccination2Dcc.withVaccinationIdentifier("IN_BLOCK_LIST")),
            Arguments.of("co", vaccination2Dcc.withVaccinationCountry("IN_BLOCK_LIST")),
            Arguments.of("dt", vaccination2Dcc.withVaccinationDate("1998-07-12"))
        )
    }

    @ParameterizedTest
    @MethodSource("vaccinationProofs")
    fun vaccine_with_one_attribute_in_rule_input_block_lists_must_return_an_error(
        fieldName: String,
        dccVaccination: GreenCertificate
    ) {
        givenExposedRules(getVaccine1_1ToVaccine2_2BlockListRule())

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(encodeToDccAndEncrypt(dccVaccination)),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
    }

    private fun testProofs(): List<Arguments> {
        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility("facility")

        return listOf(
            Arguments.of("tg", testDcc.withTestTarget("IN_BLOCK_LIST")),
            Arguments.of("tt", testDcc.withTestType("IN_BLOCK_LIST")),
            Arguments.of("nm", testDcc.withTestNameNaa("IN_BLOCK_LIST")),
            Arguments.of("ma", testDcc.withTestNameRat("IN_BLOCK_LIST")),
            Arguments.of("sc", testDcc.withTestDateTimeSample(Instant.parse("1850-01-01T10:10:10Z"))),
            Arguments.of("dr", testDcc.withTestDateTimeResult(Instant.parse("1750-01-01T10:10:10Z"))),
            Arguments.of("tr", testDcc.withTestResult("IN_BLOCK_LIST")),
            Arguments.of("tc", testDcc.withTestFacility("IN_BLOCK_LIST")),
            Arguments.of("is", testDcc.withTestIssuer("IN_BLOCK_LIST")),
            Arguments.of("ci", testDcc.withTestIdentifier("IN_BLOCK_LIST")),
            Arguments.of("co", testDcc.withTestCountry("IN_BLOCK_LIST"))
        )
    }

    @ParameterizedTest
    @MethodSource("testProofs")
    fun test_with_one_attribute_in_rule_input_block_lists_must_return_an_error(
        fieldName: String,
        testProof: GreenCertificate
    ) {
        givenExposedRules(getTestToTestBlockListRule())

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(encodeToDccAndEncrypt(testProof)),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
    }

    @Test
    fun test_with_no_attribute_in_rule_input_block_lists_must_return_a_new_certificate() {
        givenExposedRules(getTestToTestBlockListRule())

        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility("facility")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
    }

    private fun recoveryProofs(): List<Arguments> {
        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val recoveryDcc = recoveryStatementCertificate(testDate)

        return listOf(
            Arguments.of("tg", recoveryDcc.withRecoveryTarget("IN_BLOCK_LIST")),
            Arguments.of("fr", recoveryDcc.withRecoveryDateOfFirstPositiveTestResult(LocalDate.parse("1650-01-01"))),
            Arguments.of("co", recoveryDcc.withRecoveryCountry("IN_BLOCK_LIST")),
            Arguments.of("is", recoveryDcc.withRecoveryIssuer("IN_BLOCK_LIST")),
            Arguments.of("df", recoveryDcc.withRecoveryValidFrom(LocalDate.parse("1850-01-01"))),
            Arguments.of("du", recoveryDcc.withRecoveryValidUntil(LocalDate.parse("1750-01-01"))),
            Arguments.of("ci", recoveryDcc.withRecoveryIdentifier("IN_BLOCK_LIST"))
        )
    }

    @ParameterizedTest
    @MethodSource("recoveryProofs")
    fun recovery_with_one_attribute_in_rule_input_block_lists_must_return_an_error(
        fieldName: String,
        recoveryProof: GreenCertificate
    ) {
        givenExposedRules(getRecoveryToTestBlockListRule())

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(recoveryProof),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
    }

    @Test
    fun recovery_with_no_attribute_in_rule_input_block_lists_must_return_a_new_certificate() {
        givenExposedRules(getRecoveryToTestBlockListRule())

        val testDate = Instant.parse("1899-10-10T10:10:10Z")

        val recoveryDcc = recoveryStatementCertificate(testDate)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(recoveryDcc),
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
    }
}
