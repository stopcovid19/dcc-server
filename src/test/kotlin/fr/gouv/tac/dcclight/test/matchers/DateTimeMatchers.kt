package fr.gouv.tac.dcclight.test.matchers

import kotlinx.datetime.Instant
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeDiagnosingMatcher
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

/**
 * Creates a matcher that matches if the examined Instant is almost equal to the expected Instant with some error tolerance.
 * For example:
 *
 *     val expected = Instant.parse("2022-03-30T09:30:00Z")
 *     val actual =   Instant.parse("2022-03-30T09:35:00Z")
 *     assertThat(actual, isNear(expected, 10.minutes)
 */
fun isNear(instant: Instant, offset: Duration): Matcher<Instant> = IsInstantMatcher(instant, offset)

private class IsInstantMatcher(private val instant: Instant, private val offset: Duration = 0.seconds) :
    TypeSafeDiagnosingMatcher<Instant>() {

    override fun describeTo(description: Description) {
        description
            .appendText("an instant near ")
            .appendValue(instant)
            .appendText(" +/- $offset")
    }

    override fun matchesSafely(actualInstant: Instant, mismatchDescription: Description): Boolean {
        val lowerBound = instant.minus(offset)
        val upperBound = instant.plus(offset)
        mismatchDescription
            .appendText("was ")
            .appendValue(actualInstant)
        return if (actualInstant < lowerBound) {
            mismatchDescription.appendText(" which is before lower bound $lowerBound")
            false
        } else if (upperBound < actualInstant) {
            mismatchDescription.appendText(" which is after upper bound $upperBound")
            false
        } else {
            true
        }
    }
}
