package fr.gouv.tac.dcclight.test.matchers

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.spotify.hamcrest.jackson.IsJsonObject
import com.spotify.hamcrest.jackson.IsJsonText.jsonText
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeDiagnosingMatcher

/**
 * Creates a matcher that matches if the examined ByteArray is a json String.
 * For example:
 *
 *     assertThat("{ \"ver\": 1 } ".toByteArray(), isJson( [jsonObject() matcher] )
 */
fun isJson(matcher: Matcher<JsonNode>): TypeSafeDiagnosingMatcher<ByteArray> = IsByteArrayJsonObjectMatcher(matcher)

/**
 * Adds a verification on the IsJsonObject matcher to ensure attribute <code>key</code> is equal to <code>value</code>.
 * @param key the json attribute name
 * @param value the expected value
 */
fun IsJsonObject.where(key: String, value: String): IsJsonObject = this.where(key, jsonText(value))

/**
 * Adds a verification on the IsJsonObject matcher to ensure attribute <code>key</code> matches the given String matcher.
 * @param key the json attribute name
 * @param matcher a string matcher
 */
fun IsJsonObject.where(key: String, matcher: Matcher<String>): IsJsonObject = this.where(key, jsonText(matcher))

private class IsByteArrayJsonObjectMatcher(private val nextMatcher: Matcher<JsonNode>) :
    TypeSafeDiagnosingMatcher<ByteArray>() {

    override fun describeTo(description: Description) {
        description
            .appendText("a json object containing ")
            .appendDescriptionOf(nextMatcher)
    }

    override fun matchesSafely(item: ByteArray, mismatchDescription: Description): Boolean {
        try {
            val jsonNode: JsonNode = jacksonObjectMapper()
                .readTree(item)
            mismatchDescription.appendText("was a json object containing ")
            nextMatcher.describeMismatch(jsonNode, mismatchDescription)
            return nextMatcher.matches(jsonNode)
        } catch (e: JsonParseException) {
            mismatchDescription.appendText("was not a valid json ")
            mismatchDescription.appendValue(item.decodeToString())
            mismatchDescription.appendText(" because of parse error ${e.message}")
        }
        return false
    }
}
