package fr.gouv.tac.dcclight.test

import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.ANNOTATION_CLASS
import kotlin.annotation.AnnotationTarget.CLASS

@ActiveProfiles("dev")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@Retention(RUNTIME)
@Target(ANNOTATION_CLASS, CLASS)
@TestExecutionListeners(
    // order is important: encryptionAndSigningKeysManager must execute his method beforeTestClass before NettyManager
    listeners = [
        RestAssuredManager::class,
        TransportEncryptionKeysManager::class,
        DccSigningKeysManager::class,
        NettyManager::class,
        MetricsManager::class
    ],
    mergeMode = MERGE_WITH_DEFAULTS
)
@DisplayNameGeneration(ReplaceUnderscores::class)
annotation class IntegrationTest
