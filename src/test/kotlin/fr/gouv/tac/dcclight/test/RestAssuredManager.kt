package fr.gouv.tac.dcclight.test

import com.fasterxml.jackson.databind.JsonNode
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.TEST_TRANSPORT_KEYS
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.matchers.isBase64Encoded
import fr.gouv.tac.dcclight.test.matchers.isEncrypted
import fr.gouv.tac.dcclight.test.matchers.isJson
import io.restassured.RestAssured
import io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails
import io.restassured.RestAssured.given
import io.restassured.http.ContentType.JSON
import io.restassured.response.ValidatableResponse
import io.restassured.specification.RequestSpecification
import org.hamcrest.Matcher
import org.hamcrest.MatcherAssert
import org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE
import org.springframework.test.context.TestContext
import org.springframework.test.context.TestExecutionListener
import java.util.Locale.US

class RestAssuredManager : TestExecutionListener {

    companion object {
        fun givenBaseHeaders(): RequestSpecification {
            return given()
                .accept(JSON)
                .contentType(JSON)
                .header(ACCEPT_LANGUAGE, US)
        }

        fun givenAggregationRequestWith(vararg certificates: GreenCertificate) =
            givenBaseHeaders()
                .body(
                    DccAggregationRequest(
                        publicKey = CLIENT_PUBLIC_KEY_BASE64,
                        certificates = certificates.map(::encodeToDccAndEncrypt)
                    )
                )

        fun configureRestAssured(baseUrl: String) {
            RestAssured.baseURI = baseUrl
            enableLoggingOfRequestAndResponseIfValidationFails()
        }

        fun RequestSpecification.When(): RequestSpecification {
            return this.`when`()
        }

        fun ValidatableResponse.bodyEncrypted(path: String, matcher: Matcher<JsonNode>): ValidatableResponse {
            val encryptedAttributeValue = this.extract()
                .body()
                .jsonPath()
                .getString(path)
            MatcherAssert.assertThat(
                encryptedAttributeValue,
                isBase64Encoded(isEncrypted(isJson(matcher), TEST_TRANSPORT_KEYS))
            )
            return this
        }
    }

    override fun beforeTestMethod(testContext: TestContext) {
        val port = testContext.applicationContext.environment.getRequiredProperty("local.server.port")
        configureRestAssured("http://localhost:$port")
    }
}
