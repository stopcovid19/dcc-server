package fr.gouv.tac.dcclight.test

import fr.gouv.tac.dcclight.AES_GCM_CIPHER_TYPE
import fr.gouv.tac.dcclight.AES_GCM_IV_LENGTH
import fr.gouv.tac.dcclight.AES_GCM_TAG_LENGTH_IN_BITS
import fr.gouv.tac.dcclight.ALGORITHM_AES
import fr.gouv.tac.dcclight.ALGORITHM_ECDH
import fr.gouv.tac.dcclight.CONVERSION_STRING_INPUT
import fr.gouv.tac.dcclight.HASH_HMACSHA256
import fr.gouv.tac.dcclight.NAMED_CURVE_SPEC
import fr.gouv.tac.dcclight.extension.safeUse
import fr.gouv.tac.dcclight.extension.use
import fr.gouv.tac.dcclight.test.extension.base64Encode
import org.springframework.test.context.TestExecutionListener
import java.io.ByteArrayOutputStream
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.PublicKey
import java.security.SecureRandom
import java.security.spec.ECGenParameterSpec
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.KeyAgreement
import javax.crypto.Mac
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec

class TransportEncryptionKeysManager : TestExecutionListener {

    companion object {

        private val SERVER_KEY_PAIR = createECDHKeyPair()
        private val CLIENT_APP_KEY_PAIR = createECDHKeyPair()

        /**
         * client / server key exchange for transport encryption
         */
        val TEST_TRANSPORT_KEYS = TransportKeys(
            appKey = CLIENT_APP_KEY_PAIR.private,
            serverKey = SERVER_KEY_PAIR.public
        )

        val CLIENT_PUBLIC_KEY_BASE64 = CLIENT_APP_KEY_PAIR.public.encoded.base64Encode()

        init {
            System.setProperty("dcclight.encryption-private-key", SERVER_KEY_PAIR.private.encoded.base64Encode())
        }

        fun encryptForServer(clearData: ByteArray, transportKeys: TransportKeys = TEST_TRANSPORT_KEYS): ByteArray =
            SecretKeySpec(transportKeys.getKeyAgreement(), ALGORITHM_AES).safeUse { secretKey ->
                val bos = ByteArrayOutputStream()

                val cipher = Cipher.getInstance(AES_GCM_CIPHER_TYPE)
                cipher.init(Cipher.ENCRYPT_MODE, secretKey)
                bos.write(cipher.iv)
                CipherOutputStream(bos, cipher).use { cos -> cos.write(clearData) }

                bos.toByteArray()
            }

        fun decryptServerResponse(encryptedData: ByteArray, transportKeys: TransportKeys = TEST_TRANSPORT_KEYS): ByteArray {
            val cipher = Cipher.getInstance(AES_GCM_CIPHER_TYPE)
            val iv: ByteArray = encryptedData.copyOfRange(0, AES_GCM_IV_LENGTH)
            val ivSpec = GCMParameterSpec(AES_GCM_TAG_LENGTH_IN_BITS, iv)

            return SecretKeySpec(transportKeys.getKeyAgreement(), ALGORITHM_AES).safeUse { key ->
                cipher.init(Cipher.DECRYPT_MODE, key, ivSpec)
                cipher.doFinal(encryptedData, AES_GCM_IV_LENGTH, encryptedData.size - AES_GCM_IV_LENGTH)
            }
        }

        private fun createECDHKeyPair(): KeyPair {
            val ecSpec = ECGenParameterSpec(NAMED_CURVE_SPEC)
            val keyPairGenerator = KeyPairGenerator.getInstance("EC")
            keyPairGenerator.initialize(ecSpec, SecureRandom())
            return keyPairGenerator.generateKeyPair()
        }
    }

    class TransportKeys(
        private val appKey: PrivateKey,
        private val serverKey: PublicKey
    ) {
        fun getKeyAgreement(): ByteArray {
            val keyAgreement = KeyAgreement.getInstance(ALGORITHM_ECDH)

            keyAgreement.init(appKey)
            keyAgreement.doPhase(serverKey, true)

            return keyAgreement.generateSecret().use { sharedSecret ->
                SecretKeySpec(sharedSecret, HASH_HMACSHA256).safeUse { secretKeySpec ->
                    val hmac = Mac.getInstance(HASH_HMACSHA256)
                    hmac.init(secretKeySpec)
                    hmac.doFinal(CONVERSION_STRING_INPUT.toByteArray(Charsets.UTF_8))
                }
            }
        }
    }
}
