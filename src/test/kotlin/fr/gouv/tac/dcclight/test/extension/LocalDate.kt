package fr.gouv.tac.dcclight.test.extension

import java.text.SimpleDateFormat
import java.util.Date

fun kotlinx.datetime.LocalDate.toJavaDate(): Date = SimpleDateFormat("yyyy-MM-dd").parse(this.toString())
