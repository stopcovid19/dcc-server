package fr.gouv.tac.dcclight.test.matchers

import com.spotify.hamcrest.jackson.JsonMatchers.jsonObject
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDcc
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.LocalDate
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.matchesRegex
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.minutes

class HcertEncodedMatcherTest {

    private val encodedDcc = encodeToDcc(
        pfizer1_1VaccinationCertificate(LocalDate(2020, 3, 30))
    )

    @Test
    fun can_check_issuedAt_and_expirationTime() {
        assertThat(
            encodedDcc.step5Prefixed,
            isHcertEncoded(
                issuedBy = DCC_TEST_INPUT_KEYS,
                issuedAt = isNear(now(), 1.minutes),
                expirationTime = isNear(now().plus(3650.days), 1.minutes)
            )
        )
    }

    @Test
    fun can_detect_wrong_issuedAt() {
        val error: AssertionError = assertThrows {
            assertThat(
                encodedDcc.step5Prefixed,
                isHcertEncoded(
                    issuedBy = DCC_TEST_INPUT_KEYS,
                    issuedAt = isNear(now().plus(10.days), 1.minutes), // should be now
                    expirationTime = isNear(now().plus(3650.days), 1.minutes)
                )
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            matchesRegex(
                """

                    Expected: an HCERT encoded string issued by certificate <Test certificate for generated input> at an instant near <[0-9-:.T]+Z> \+/- 1m and expiring on an instant near <[0-9-:.T]+Z> \+/- 1m and euDcc \{
                    }
                         but: issuedAt was <[0-9-:.T]+Z> and expirationTime was <[0-9-:.T]+Z> and euDcc 
                """.trimIndent()
            )
        )
    }

    @Test
    fun can_detect_wrong_expirationTime() {
        val error: AssertionError = assertThrows {
            assertThat(
                encodedDcc.step5Prefixed,
                isHcertEncoded(
                    issuedBy = DCC_TEST_INPUT_KEYS,
                    issuedAt = isNear(now(), 1.minutes),
                    expirationTime = isNear(now(), 1.minutes) // should be now + 10 years
                )
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            matchesRegex(
                """

                    Expected: an HCERT encoded string issued by certificate <Test certificate for generated input> at an instant near <[0-9-:.T]+Z> \+/- 1m and expiring on an instant near <[0-9-:.T]+Z> \+/- 1m and euDcc \{
                    }
                         but: issuedAt was <[0-9-:.T]+Z> and expirationTime was <[0-9-:.T]+Z> and euDcc 
                """.trimIndent()
            )
        )
    }

    @Test
    fun can_detect_wrong_eudcc() {
        val error: AssertionError = assertThrows {
            assertThat(
                encodedDcc.step5Prefixed,
                isHcertEncoded(
                    issuedBy = DCC_TEST_INPUT_KEYS,
                    issuedAt = isNear(now(), 1.minutes),
                    expirationTime = isNear(now().plus(3650.days), 1.minutes),
                    euDcc = jsonObject()
                        .where("ver", "wrong version")
                )
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            matchesRegex(
                """

                    Expected: an HCERT encoded string issued by certificate <Test certificate for generated input> at an instant near <[0-9-:.T]+Z> \+/- 1m and expiring on an instant near <[0-9-:.T]+Z> \+/- 1m and euDcc \{
                      "ver": a text node with value that is "wrong version"
                    }
                         but: issuedAt was <[0-9-:.T]+Z> and expirationTime was <[0-9-:.T]+Z> and euDcc \{
                      "ver": was a text node with value that was "1\.3\.0"
                    }
                """.trimIndent()
            )
        )
    }

    @Test
    fun can_detect_wrong_signature() {
        val error: AssertionError = assertThrows {
            assertThat(
                encodedDcc.step5Prefixed,
                isHcertEncoded(
                    issuedBy = DCC_SERVER_KEYS,
                    issuedAt = isNear(now(), 1.minutes),
                    expirationTime = isNear(now().plus(3650.days), 1.minutes),
                    euDcc = jsonObject()
                        .where("ver", "wrong version")
                )
            )
        }
        assertThat(
            error.message?.replace("\r", ""),
            matchesRegex(
                """

                    Expected: an HCERT encoded string issued by certificate <Running DCC server certificate> at an instant near <[0-9-:.T]+Z> \+/- 1m and expiring on an instant near <[0-9-:.T]+Z> \+/- 1m and euDcc \{
                      "ver": a text node with value that is "wrong version"
                    }
                         but: was not signed by <Running DCC server certificate>
                """.trimIndent()
            )
        )
    }
}
