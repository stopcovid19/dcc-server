package fr.gouv.tac.dcclight.test

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import ehn.techiop.hcert.kotlin.data.Person
import ehn.techiop.hcert.kotlin.data.RecoveryStatement
import ehn.techiop.hcert.kotlin.data.Test
import ehn.techiop.hcert.kotlin.data.Vaccination
import ehn.techiop.hcert.kotlin.data.ValueSetEntry
import ehn.techiop.hcert.kotlin.data.ValueSetEntryAdapter
import fr.gouv.tac.dcclight.rules.CertificateTarget
import fr.gouv.tac.dcclight.rules.TestResult
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.DateTimeUnit.Companion.DAY
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone.Companion.currentSystemDefault
import kotlinx.datetime.minus
import kotlinx.datetime.plus
import kotlinx.datetime.toLocalDateTime
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.DurationUnit.DAYS

fun pfizer1_1VaccinationCertificate(vaccinationDate: LocalDate) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    vaccinations = arrayOf(pfizerVaccination_1_1(vaccinationDate = vaccinationDate))
)

fun pfizer2_2VaccinationCertificate(vaccinationDate: LocalDate) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    vaccinations = arrayOf(pfizerVaccination_2_2(vaccinationDate = vaccinationDate))
)

fun pfizer1_2VaccinationCertificate(date: LocalDate) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    vaccinations = arrayOf(pfizerVaccination_1_2(vaccinationDate = date))
)

fun janssen1_2VaccinationCertificate(date: LocalDate) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    vaccinations = arrayOf(janssenVaccination_1_2(vaccinationDate = date))
)

fun positiveAntigenicTestCertificate(date: Instant) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    tests = arrayOf(antigenicPositiveTest(date))
)

fun negativeAntigenicTestCertificate(date: Instant) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    tests = arrayOf(antigenicNegativeTest(date))
)

fun negativePcrTestCertificate(date: Instant) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    tests = arrayOf(pcrNegativeTest(date))
)

fun positivePcrTestCertificate(date: Instant) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    tests = arrayOf(pcrPositiveTest(date))
)

fun covid19RecoveryStatement(dateOfFirstPositiveTestResult: Instant) = RecoveryStatement(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    dateOfFirstPositiveTestResult = dateOfFirstPositiveTestResult
        .toLocalDateTime(currentSystemDefault()).date,
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:AZIPDJAZEIFHB23#5",
    certificateValidUntil = now().plus(300.days).toLocalDateTime(currentSystemDefault()).date,
    certificateValidFrom = now().toLocalDateTime(currentSystemDefault()).date
)

fun recoveryStatementCertificate(recoveryDate: Instant) = GreenCertificate(
    schemaVersion = "1.3.0",
    subject = Person(
        familyName = "Dupont",
        familyNameTransliterated = "DUPONT",
        givenNameTransliterated = "JEROME",
        givenName = "Jérôme"
    ),
    dateOfBirthString = LocalDate(1955, 5, 5).toString(),
    recoveryStatements = arrayOf(covid19RecoveryStatement(recoveryDate))
)

fun pfizerVaccination_1_1(vaccinationDate: LocalDate) = Vaccination(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    vaccine = entryOf("1119349007"),
    medicinalProduct = entryOf("EU/1/20/1528"),
    authorizationHolder = entryOf("ORG-100030215"),
    doseNumber = 1,
    doseTotalNumber = 1,
    date = vaccinationDate,
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:AUF72H4HV8ANCTW#2"
)

fun pfizerVaccination_1_2(vaccinationDate: LocalDate) = Vaccination(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    vaccine = entryOf("1119349007"),
    medicinalProduct = entryOf("EU/1/20/1528"),
    authorizationHolder = entryOf("ORG-100030215"),
    doseNumber = 1,
    doseTotalNumber = 2,
    date = vaccinationDate,
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:AUF72H4HV8ANCTW#2"
)

fun pfizerVaccination_2_2(vaccinationDate: LocalDate) = Vaccination(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    vaccine = entryOf("1119349007"),
    medicinalProduct = entryOf("EU/1/20/1528"),
    authorizationHolder = entryOf("ORG-100030215"),
    doseNumber = 2,
    doseTotalNumber = 2,
    date = vaccinationDate,
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:OO38V701OUQSGYC#K"
)

fun janssenVaccination_1_2(vaccinationDate: LocalDate) = Vaccination(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    vaccine = entryOf("J07BX03"),
    medicinalProduct = entryOf("EU/1/20/1525"),
    authorizationHolder = entryOf("ORG-100001417"),
    doseNumber = 1,
    doseTotalNumber = 2,
    date = vaccinationDate,
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:OO38V701OUQSGYC#K"
)

fun antigenicPositiveTest(sampleDate: Instant) = Test(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    type = entryOf("LP217198-3"),
    nameRat = entryOf("1232"),
    dateTimeSample = sampleDate,
    resultPositive = entryOf(TestResult.DETECTED.EUValue),
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:1OAZ971234890NB#1"
)

fun antigenicNegativeTest(sampleDate: Instant) = Test(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    type = entryOf("LP217198-3"),
    nameRat = entryOf("1232"),
    dateTimeSample = sampleDate,
    resultPositive = entryOf(TestResult.NOT_DETECTED.EUValue),
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:1OAZ971234890NB#Z"
)

fun pcrPositiveTest(sampleDate: Instant) = Test(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    type = entryOf("LP6464-4"),
    dateTimeSample = sampleDate,
    resultPositive = entryOf(TestResult.DETECTED.EUValue),
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:12PIBHCGYF232A7#6"
)

fun pcrNegativeTest(sampleDate: Instant) = Test(
    target = entryOf(CertificateTarget.COVID_19.EUValue),
    type = entryOf("LP6464-4"),
    dateTimeSample = sampleDate,
    resultPositive = entryOf(TestResult.NOT_DETECTED.EUValue),
    country = "FR",
    certificateIssuer = "CNAM",
    certificateIdentifier = "URN:UVCI:01:FR:AZ9R8GHFPQSO6B8#L"
)

fun entryOf(key: String) = ValueSetEntryAdapter(
    key = key,
    valueSetEntry = ValueSetEntry(
        display = "",
        lang = "",
        active = true,
        system = "",
        version = ""
    )
)

/**
 * Returns a local date for today +/- given offsets. Default is no offset.
 *
 * @param minus the amount of time to remove
 * @param plus the amount of time to add
 */
fun today(minus: Duration = 0.days, plus: Duration = 0.days) = now()
    .toLocalDateTime(currentSystemDefault())
    .date
    .minus(minus.toInt(DAYS), DAY)
    .plus(plus.toInt(DAYS), DAY)
