package fr.gouv.tac.dcclight.test

import ehn.techiop.hcert.kotlin.crypto.CertificateAdapter
import ehn.techiop.hcert.kotlin.crypto.JvmPrivKey
import ehn.techiop.hcert.kotlin.crypto.JvmPubKey
import ehn.techiop.hcert.kotlin.crypto.PkiUtils
import ehn.techiop.hcert.kotlin.trust.ContentType
import fr.gouv.tac.dcclight.NAMED_CURVE_SPEC
import fr.gouv.tac.dcclight.test.extension.base64Decode
import fr.gouv.tac.dcclight.test.extension.base64Encode
import kotlinx.datetime.Clock
import org.springframework.test.context.TestExecutionListener
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.SecureRandom
import java.security.spec.ECGenParameterSpec
import java.security.spec.PKCS8EncodedKeySpec

class DccSigningKeysManager : TestExecutionListener {

    companion object {

        /**
         * The keys used to sign DCC sent in test requests.
         */
        val DCC_TEST_INPUT_KEYS = generateRandomKeys("Test certificate for generated input")

        /**
         * The keys used by the running DCC server to sign DCC returned by the API.
         */
        val DCC_SERVER_KEYS = generateRandomKeys("Running DCC server certificate")

        /**
         * The keys used by the running DCC server to sign DCCLight returned by the API.
         */
        val DCCLIGHT_SERVER_KEYS = generateRandomKeys("Running DCC light server certificate")

        init {
            System.setProperty("dcclight.dcc.signing-private-key", DCC_SERVER_KEYS.privateKeyAsBase64String())
            System.setProperty("dcclight.dcc.signing-certificate", DCC_SERVER_KEYS.certificateAsPem())

            System.setProperty("dcclight.dcclight.signing-private-key", DCCLIGHT_SERVER_KEYS.privateKeyAsBase64String())
            System.setProperty("dcclight.dcclight.signing-certificate", DCCLIGHT_SERVER_KEYS.certificateAsPem())
        }

        private fun generateRandomKeys(label: String): DccSigningKeys {
            val keyPair = KeyPairGenerator.getInstance("EC")
                .apply { initialize(ECGenParameterSpec(NAMED_CURVE_SPEC), SecureRandom()) }
                .generateKeyPair()
            val privateKey = keyPair.private
            val certificate = PkiUtils.selfSignCertificate(
                JvmPrivKey(keyPair.private),
                JvmPubKey(keyPair.public),
                256,
                ContentType.values().toList(),
                Clock.System
            )
            return DccSigningKeys(label, privateKey, certificate)
        }
    }

    class DccSigningKeys(
        val label: String,
        private val privateKey: PrivateKey,
        private val certificate: CertificateAdapter
    ) {

        constructor(label: String, privateKey: String, certificatePem: String) : this(
            label,
            KeyFactory.getInstance("EC")
                .generatePrivate(PKCS8EncodedKeySpec(privateKey.base64Decode())),
            CertificateAdapter(certificatePem)
        )

        fun privateKeyAsBase64String() = privateKey.encoded.base64Encode()

        fun privateKeyAsPem(): String {
            val base64PrivateKey = privateKeyAsBase64String()
            return """
                -----BEGIN PRIVATE KEY-----
                $base64PrivateKey
                -----END PRIVATE KEY-----
            """.trimIndent()
        }

        fun kid() = certificate.kid

        fun certificateAsBase64() = certificate.encoded.base64Encode()

        fun certificateAsPem(): String {
            val base64Certificate = certificateAsBase64()
            return """
                -----BEGIN CERTIFICATE-----
                $base64Certificate
                -----END CERTIFICATE-----
            """.trimIndent()
        }

        override fun toString(): String {
            return label
        }
    }
}
