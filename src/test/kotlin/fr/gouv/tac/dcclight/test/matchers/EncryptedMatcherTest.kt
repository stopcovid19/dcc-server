package fr.gouv.tac.dcclight.test.matchers

import fr.gouv.tac.dcclight.AES_GCM_IV_LENGTH
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.encryptForServer
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import javax.crypto.AEADBadTagException

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores::class)
class EncryptedMatcherTest {

    @Test
    fun can_unwrap_encrypted_data() {
        val encryptedData = encryptForServer("DCC Server".toByteArray())
        assertThat(encryptedData, isEncrypted(equalTo("DCC Server".toByteArray())))
        assertThat(encryptedData, isEncrypted(not(equalTo("other server".toByteArray()))))
    }

    @Test
    fun can_detect_input_is_not_encrypted_because_it_is_too_short() {
        val byteArray = ByteArray(3)
        val error: IndexOutOfBoundsException = assertThrows {
            assertThat(byteArray, isEncrypted(equalTo(byteArray)))
        }
        assertThat(error.message, equalTo("toIndex (12) is greater than size (3)."))
    }

    @Test
    fun can_detect_input_is_not_properly_encrypted() {
        val byteArray = ByteArray(AES_GCM_IV_LENGTH)
        val error: AEADBadTagException = assertThrows {
            assertThat(byteArray, isEncrypted(equalTo(byteArray)))
        }
        assertThat(error.message, equalTo("Input too short - need tag"))
    }
}
