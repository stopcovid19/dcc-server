package fr.gouv.tac.dcclight.test.matchers

import com.fasterxml.jackson.databind.JsonNode
import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import com.spotify.hamcrest.jackson.JsonMatchers.jsonArray
import com.spotify.hamcrest.jackson.JsonMatchers.jsonInt
import com.spotify.hamcrest.jackson.JsonMatchers.jsonText
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.matchesRegex
import org.hamcrest.TypeSafeDiagnosingMatcher

/**
 * Creates a matcher that matches if the examined JsonNode is a json String representing an EU DCC.
 * For example:
 *
 *     val json = """
 *         {
 *           "ver": "1.3.0",
 *           "dob": "1955-05-05",
 *           "nam": {
 *             "fn": "Dupont",
 *             "fnt": "DUPONT",
 *             "gn": "Jérôme",
 *             "fnt": "JEROME"
 *           },
 *           "v": {
 *             "du": "2020-07-22",
 *             ...
 *           }
 *         }
 *     """
 *     val jsonNode = jacksonObjectMapper().readTree(json)
 *     assertThat(jsonNode, isEuDcc()
 *             .forJeromeDupont()
 *             .withProofType("v")
 *             .where("du", LocalDate(2020, 7, 22))
 *     )
 */
fun isEuDcc() = EuDccMatcher()

fun matchesUvciPattern(): Matcher<String> = matchesRegex("^URN:UVCI:01:FR:DGSAG/[A-Z0-9]{40,49}#[A-Z0-9:/]$")

class EuDccMatcher : TypeSafeDiagnosingMatcher<JsonNode>() {

    private var euDccJsonMatcher = jsonObject()

    private var proofType: String? = null

    private var proofMatcher = jsonObject()

    private fun buildMatcher(): Matcher<JsonNode> {
        if (null == proofType) {
            return euDccJsonMatcher
        }
        return euDccJsonMatcher.where(
            proofType, jsonArray(contains(proofMatcher))
        )
    }

    override fun describeTo(description: Description) {
        description.appendText(" json containing ")
        buildMatcher().describeTo(description)
    }

    override fun matchesSafely(euDccJson: JsonNode, mismatchDescription: Description): Boolean {
        mismatchDescription
            .appendText("was json ")
            .appendText(euDccJson.toPrettyString())
            .appendText(" with differences ")
        val matcher = buildMatcher()
        matcher.describeMismatch(euDccJson, mismatchDescription)
        return matcher.matches(euDccJson)
    }

    fun forJeromeDupont() = forJeromeDupontWith()

    fun forJeromeDupontWith(
        dob: String = "1955-05-05",
        fnt: String = "DUPONT",
        fn: String = "Dupont",
        gnt: String = "JEROME",
        gn: String = "Jérôme",
        ver: String = "1.3.0"
    ): EuDccMatcher {
        euDccJsonMatcher = euDccJsonMatcher
            .where("ver", ver)
            .where("dob", dob)
            .where(
                "nam",
                jsonObject()
                    .where("fnt", fnt)
                    .where("fn", fn)
                    .where("gnt", gnt)
                    .where("gn", gn)
            )
        return this
    }

    /**
     * Sets the expected proof attribute.
     * @param proofType the expected proof type ("t", "v" or "r")
     */
    fun withProofType(proofType: String): EuDccMatcher {
        this.proofType = proofType
        return this
    }

    /**
     * Ensure an attribute of the proof has a given value.
     * @param key the name of the proof attribute
     * @param value the expected value of the proof
     */
    fun where(key: String, value: Any): EuDccMatcher {
        proofMatcher = proofMatcher.where(key, jsonText(value.toString()))
        return this
    }

    /**
     * Ensure an attribute of the proof has a given value.
     * @param key the name of the proof attribute
     * @param value the expected value of the proof
     */
    fun where(key: String, value: Int): EuDccMatcher {
        proofMatcher = proofMatcher.where(key, jsonInt(value))
        return this
    }

    /**
     * Ensure an attribute of the proof has a given value.
     * @param key the name of the proof attribute
     * @param matcher a matcher the proof attribut value must match
     */
    fun where(key: String, matcher: Matcher<String>): EuDccMatcher {
        proofMatcher = proofMatcher.where(key, jsonText(matcher))
        return this
    }
}
