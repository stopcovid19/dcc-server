package fr.gouv.tac.dcclight.test.extension

import fr.gouv.tac.dcclight.rules.model.YamlInputRule
import fr.gouv.tac.dcclight.rules.model.YamlOutputRule
import fr.gouv.tac.dcclight.rules.model.YamlRules

fun YamlRules.addInputRule(testRule: YamlInputRule) = this.copy(
    rules = listOf(rules[0].copy(input = this.rules[0].input.plus(testRule)))
)

fun YamlRules.withOutputRule(outputRule: YamlOutputRule) = this.copy(
    rules = listOf(rules[0].copy(output = outputRule))
)

fun YamlInputRule.withAllowListItem(key: String, value: List<String>) =
    this.copy(allowLists = HashMap<String, List<String>>(this.allowLists).apply { this[key] = value })

fun YamlInputRule.withBlockListItem(key: String, value: List<String>) =
    this.copy(blockLists = HashMap<String, List<String>>(this.blockLists).apply { this[key] = value })
