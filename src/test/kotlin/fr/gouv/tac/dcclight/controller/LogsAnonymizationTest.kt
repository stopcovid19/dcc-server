package fr.gouv.tac.dcclight.controller

import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.extension.getVaccination
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.negativeAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.TimeZone
import kotlinx.datetime.atStartOfDayIn
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.system.CapturedOutput
import org.springframework.boot.test.system.OutputCaptureExtension
import org.springframework.http.HttpStatus
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

@ExtendWith(OutputCaptureExtension::class)
@IntegrationTest
class LogsAnonymizationTest {

    @Test
    fun should_anonymize_complete_logs_when_no_corresponding_aggregation_rule(output: CapturedOutput) {

        val vaccinationDcc = pfizer1_1VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))

        assertThat(output.all).contains(
            "DCC type: v",
            "position: 0",
            "vaccine code: SARS-CoV-2 mRNA vaccine",
            "vaccine type: Comirnaty",
            "vaccine manufacturer: Biontech Manufacturing GmbH",
            "dose number: 1",
            "total dose number: 1",
            "country: FR",
            "vaccination date: ${today(minus = 25.days)}",
            "vaccination certificate issuer: CNAM"
        )
        assertThat(output.all).contains(
            "DCC type: t",
            "position: 1",
            "test type: Rapid immunoassay",
            "test name: null",
            "test device: Abbott Rapid Diagnostics, Panbio COVID-19 Ag Test",
            "test date result: null",
            "test result: Not detected",
            "test country: ",
            "test certificate issuer: CNAM"
        )
        assertThat(output.all).doesNotContain(
            "reference family name transliterated:",
            "DCC wrong family name transliterated:",
            "reference family name:",
            "DCC wrong family name:",
            "reference given name transliterated:",
            "DCC wrong given name transliterated:",
            "reference given name:",
            "DCC wrong given name:",
            "reference date of birth:",
            "DCC wrong date of birth:",
        )
    }

    @Test
    fun should_anonymize_logs_when_one_entry_is_in_the_future_more_than_1_day_or_24_hours(output: CapturedOutput) {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 20.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(plus = 10.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(vaccination2Dcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE"))
            .body("errors[0].message", equalTo("Certificate proof date is in more than 24 hours"))
            .body("errors.size()", equalTo(1))

        assertThat(output.all).contains(
            "Certificate proof date is in more than 24 hours",
            "DCC type: v",
            "DCC date: ${vaccination2Dcc.getVaccination().date.atStartOfDayIn(TimeZone.currentSystemDefault())}"
        )
    }
}
