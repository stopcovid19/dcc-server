package fr.gouv.tac.dcclight.controller

import ehn.techiop.hcert.kotlin.chain.Error
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.extension.toLocalDate
import fr.gouv.tac.dcclight.service.MetricsService.RequestSource.DEFAULT
import fr.gouv.tac.dcclight.service.error.DccValidationError.DCC_DECRYPTION_ERROR
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_CERTIFICATE_IDENTIFIER
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_FAMILY_NAME
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_GIVEN_NAME
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCCLIGHT_SERVER_KEYS
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_TEST_INPUT_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.MetricsManager.Companion.assertThatCounter
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenCertificatesTrustListContains
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenAggregationRequestWith
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withFamilyName
import fr.gouv.tac.dcclight.test.extension.withGivenName
import fr.gouv.tac.dcclight.test.extension.withVaccinationIdentifier
import fr.gouv.tac.dcclight.test.pfizer1_1VaccinationCertificate
import fr.gouv.tac.dcclight.test.pfizer2_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.today
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Tag
import kotlinx.datetime.Clock
import kotlinx.datetime.DateTimeUnit.Companion.DAY
import kotlinx.datetime.TimeZone
import kotlinx.datetime.plus
import kotlinx.datetime.toLocalDateTime
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders.USER_AGENT
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days

@IntegrationTest
class MetricsTest(@Autowired private val meterRegistry: MeterRegistry) {

    @ParameterizedTest
    @CsvSource(
        "PassPlusCovid-V1                                   , PASSPLUSCOVID_V1",
        "okhttp/4.9.3                                       , DEFAULT",
        "TousAntiCovid/1210 CFNetwork/1331.0.7 Darwin/21.4.0, DEFAULT"
    )
    fun should_tag_metrics_source(userAgent: String, expectedTag: String) {

        val now = Clock.System.now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(
            now.toLocalDateTime(TimeZone.currentSystemDefault()).date.plus(
                1,
                DAY
            )
        )

        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))

        givenBaseHeaders()
            .header(USER_AGENT, userAgent)

            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())

        assertThatCounter(
            "dcc.request.aggregation.successful",
            Tag.of("source", expectedTag),
            Tag.of("rule name", "Positive-antigenic-test-vaccination")
        ).isOne
    }

    @Test
    fun dcc_with_bad_signature_should_return_decryption_error_and_increment_its_metric() {

        givenCertificatesTrustListContains(DCC_TEST_INPUT_KEYS)
        val now = Clock.System.now()

        val vaccinationDcc = pfizer1_1VaccinationCertificate(now.toLocalDate().plus(1, DAY))

        val testDcc = positiveAntigenicTestCertificate(now.minus(60.days))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc, dccSigningKeys = DCCLIGHT_SERVER_KEYS)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())

        assertThatCounter(
            "dcc.request.hcert.error",
            Tag.of("source", DEFAULT.toString()),
            Tag.of("type", Error.KEY_NOT_IN_TRUST_LIST.name)
        ).isEqualTo(1.0)
        assertThatCounter(
            "dcc.request.aggregation.error",
            Tag.of("type", DCC_DECRYPTION_ERROR.name),
            Tag.of("source", DEFAULT.toString())
        ).isEqualTo(1.0)
    }

    @Test
    fun metric_increments_when_one_dcc_presents_its_associated_error() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 50.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 25.days)).withGivenName(null)

        givenAggregationRequestWith(vaccinationDcc, vaccination2Dcc)
            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())

        assertThatCounter(
            "dcc.request.aggregation.error",
            Tag.of("type", MISSING_GIVEN_NAME.name),
            Tag.of("source", DEFAULT.toString())
        ).isOne
    }

    @Test
    fun metrics_increment_when_many_dccs_present_its_associated_errors() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days)).withVaccinationIdentifier("")

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 50.days)).withGivenName(null)

        val vaccination3Dcc = pfizer1_1VaccinationCertificate(today(minus = 25.days)).withFamilyName(null)

        givenAggregationRequestWith(vaccinationDcc, vaccination2Dcc, vaccination3Dcc)
            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())

        assertThatCounter(
            "dcc.request.aggregation.error",
            Tag.of("type", MISSING_FAMILY_NAME.name),
            Tag.of("source", DEFAULT.toString())
        ).isOne
        assertThatCounter(
            "dcc.request.aggregation.error",
            Tag.of("type", MISSING_GIVEN_NAME.name),
            Tag.of("source", DEFAULT.toString())
        ).isOne
        assertThatCounter(
            "dcc.request.aggregation.error",
            Tag.of("type", MISSING_CERTIFICATE_IDENTIFIER.name),
            Tag.of("source", DEFAULT.toString())
        ).isOne
    }

    @Test
    fun metric_increments_when_many_dccs_present_its_associated_error() {

        val vaccinationDcc = pfizer2_2VaccinationCertificate(today(minus = 100.days))

        val vaccination2Dcc = pfizer1_1VaccinationCertificate(today(minus = 50.days)).withFamilyName(null)

        val vaccination3Dcc = pfizer1_1VaccinationCertificate(today(minus = 25.days)).withFamilyName(null)

        givenAggregationRequestWith(vaccinationDcc, vaccination2Dcc, vaccination3Dcc)
            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())

        assertThatCounter(
            "dcc.request.aggregation.error",
            Tag.of("type", MISSING_FAMILY_NAME.name),
            Tag.of("source", DEFAULT.toString())
        ).isEqualTo(2.0)
    }
}
