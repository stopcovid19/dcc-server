package fr.gouv.tac.dcclight.controller

import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import io.restassured.http.ContentType.JSON
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus.BAD_REQUEST

@IntegrationTest
class RestExceptionHandlerTest {

    @Test
    fun should_return_bad_request_on_non_json_body() {
        givenBaseHeaders()
            .contentType(JSON)
            .body("not a valid json body")

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("status", equalTo(400))
            .body("error", equalTo("Bad Request"))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("path", equalTo("/api/v1/aggregate"))
            .body("errors[0].field", equalTo(""))
            .body("errors[0].code", equalTo("HttpMessageNotReadable"))
            .body(
                "errors[0].message",
                equalTo("Unrecognized token 'not': was expecting (JSON String, Number, Array, Object or token 'null', 'true' or 'false')")
            )
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun should_return_bad_request_on_non_nullable_json_body_primitive_property() {
        givenBaseHeaders()
            .contentType(JSON)
            .body(
                """
                {
                  "publicKey": null,
                  "certificates": []
                }
                """.trimIndent()
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("status", equalTo(400))
            .body("error", equalTo("Bad Request"))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("path", equalTo("/api/v1/aggregate"))
            .body("errors[0].field", equalTo("publicKey"))
            .body("errors[0].code", equalTo("HttpMessageNotReadable"))
            .body(
                "errors[0].message",
                equalTo("Instantiation of [simple type, class fr.gouv.tac.dcclight.api.model.DccAggregationRequest] value failed for JSON property publicKey due to missing (therefore NULL) value for creator parameter publicKey which is a non-nullable type")
            )
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun should_return_bad_request_on_non_nullable_json_body_array_property() {
        givenBaseHeaders()
            .contentType(JSON)
            .body(
                """
                {
                  "publicKey": "$CLIENT_PUBLIC_KEY_BASE64",
                  "certificates": null
                }
                """.trimIndent()
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("status", equalTo(400))
            .body("error", equalTo("Bad Request"))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("path", equalTo("/api/v1/aggregate"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("HttpMessageNotReadable"))
            .body(
                "errors[0].message",
                equalTo("Instantiation of [simple type, class fr.gouv.tac.dcclight.api.model.DccAggregationRequest] value failed for JSON property certificates due to missing (therefore NULL) value for creator parameter certificates which is a non-nullable type")
            )
            .body("errors.size()", equalTo(1))
    }
}
