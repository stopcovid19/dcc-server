package fr.gouv.tac.dcclight.controller

import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCCLIGHT_SERVER_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withFamilyNameTransliterated
import fr.gouv.tac.dcclight.test.extension.withGivenNameTransliterated
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withVaccinationDoseTotalNumber
import fr.gouv.tac.dcclight.test.extension.withVaccinationManufacturer
import fr.gouv.tac.dcclight.test.extension.withVaccinationMedicinalProduct
import fr.gouv.tac.dcclight.test.extension.withVaccinationVaccine
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.isNear
import fr.gouv.tac.dcclight.test.matchers.where
import fr.gouv.tac.dcclight.test.negativeAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.negativePcrTestCertificate
import fr.gouv.tac.dcclight.test.pfizer1_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.Clock.System.now
import org.junit.jupiter.api.Test
import org.junitpioneer.jupiter.cartesian.CartesianTest
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

@IntegrationTest
class DerogationCombinationTest {

    @CartesianTest
    fun returns_OK_with_dcclight_valid_for_less_than_24_hours_when_provided_with_valid_antigenic_test_and_vaccination_dcc(
        @CartesianTest.Values(
            strings = [
                "EU/1/20/1528",
                "EU/1/20/1507",
                "EU/1/21/1529",
                "Covishield",
                "R-Covi",
                "R-COVI",
                "EU/1/20/1618",
                "NVX-CoV2373",
                "Covid-19-recombinant"
            ]
        ) allowedVaccineMedicinalProduct: String,
        @CartesianTest.Values(
            strings = [
                "ORG-100032020",
                "ORG-100030215",
                "ORG-100031184",
                "ORG-100001699",
                "ORG-100001981",
                "ORG-100032020",
                "R-Pharm",
                "ORG-100007893",
                "FIOCRUZ",
                "Fiocruz"
            ]
        ) allowedVaccineManufacturers: String,
        @CartesianTest.Values(
            strings = [
                "1119349007",
                "J07BX03"
            ]
        ) allowedVaccineTypes: String,
        @CartesianTest.Values(
            ints = [2, 3]
        ) doseTotalNumber: Int,
        @CartesianTest.Values(
            strings = [
                "1232",
                "Autre",
                "AUTRE",
                "Autotest",
                "AUTOTEST"
            ]
        ) testManufacturer: String,
    ) {

        val testSampleInstant = now().minus(3.hours)

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 4.days))
            .withVaccinationManufacturer(allowedVaccineManufacturers)
            .withVaccinationMedicinalProduct(allowedVaccineMedicinalProduct)
            .withVaccinationDoseTotalNumber(doseTotalNumber)
            .withVaccinationVaccine(allowedVaccineTypes)

        val testDcc = negativeAntigenicTestCertificate(testSampleInstant)
            .withTestNameRat(testManufacturer)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCCLIGHT_SERVER_KEYS,
                            issuedAt = isNear(testSampleInstant, 1.minutes),
                            expirationTime = isNear(testSampleInstant.plus(1.days), 1.minutes),
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(ver = "1.0.0")
                        )
                    )
            )
    }

    @CartesianTest
    fun returns_OK_with_dcclight_valid_for_less_than_24_hours_when_provided_with_valid_pcr_test_and_vaccination_dcc(
        @CartesianTest.Values(
            strings = [
                "EU/1/20/1528",
                "EU/1/20/1507",
                "EU/1/21/1529",
                "Covishield",
                "R-Covi",
                "R-COVI",
                "EU/1/20/1618",
                "NVX-CoV2373"
            ]
        ) allowedVaccineMedicinalProduct: String,
        @CartesianTest.Values(
            strings = [
                "ORG-100032020",
                "ORG-100030215",
                "ORG-100031184",
                "ORG-100001699",
                "ORG-100001981",
                "ORG-100032020",
                "R-Pharm",
                "ORG-100007893",
                "FIOCRUZ",
                "Fiocruz"
            ]
        ) allowedVaccineManufacturers: String,
        @CartesianTest.Values(
            strings = [
                "1119349007",
                "J07BX03"
            ]
        ) allowedVaccineTypes: String,
        @CartesianTest.Values(
            ints = [2, 3]
        ) doseTotalNumber: Int
    ) {

        val testSampleInstant = now().minus(3.hours)

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 4.days))
            .withVaccinationVaccine(allowedVaccineTypes)
            .withVaccinationDoseTotalNumber(doseTotalNumber)
            .withVaccinationMedicinalProduct(allowedVaccineMedicinalProduct)
            .withVaccinationManufacturer(allowedVaccineManufacturers)

        val testDcc = negativePcrTestCertificate(testSampleInstant)
            .withTestNameRat(null)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCCLIGHT_SERVER_KEYS,
                            issuedAt = isNear(testSampleInstant, 1.minutes),
                            expirationTime = isNear(testSampleInstant.plus(24.hours), 1.minutes),
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(ver = "1.0.0")
                        )
                    )
            )
    }

    @Test
    fun returns_OK_with_dcclight_valid_for_less_than_24_hours_when_provided_with_valid_test_and_vaccination_dcc_without_trailing_chevrons_in_names() {

        val testSampleInstant = now().minus(3.hours)

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 4.days))
            .withFamilyNameTransliterated("DUPONT<<<")
            .withGivenNameTransliterated("JEROME<")

        val testDcc = negativePcrTestCertificate(testSampleInstant)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCCLIGHT_SERVER_KEYS,
                            issuedAt = isNear(testSampleInstant, 1.minutes),
                            expirationTime = isNear(testSampleInstant.plus(24.hours), 1.minutes),
                            euDcc = isEuDcc()
                                .forJeromeDupontWith(ver = "1.0.0")
                        )
                    )
            )
    }
}
