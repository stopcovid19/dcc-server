package fr.gouv.tac.dcclight.controller

import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenCountryUvciBlockListContains
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withTestType
import fr.gouv.tac.dcclight.test.extension.withVaccinationManufacturer
import fr.gouv.tac.dcclight.test.extension.withVaccinationMedicinalProduct
import fr.gouv.tac.dcclight.test.extension.withVaccinationVaccine
import fr.gouv.tac.dcclight.test.janssen1_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.negativeAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.negativePcrTestCertificate
import fr.gouv.tac.dcclight.test.pfizer1_2VaccinationCertificate
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import fr.gouv.tac.dcclight.test.positivePcrTestCertificate
import fr.gouv.tac.dcclight.test.today
import kotlinx.datetime.Clock.System.now
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus.BAD_REQUEST
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

@IntegrationTest
class DerogationErrorsTest {

    @Test
    fun positive_compliant_antigenic_test_from_less_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = positiveAntigenicTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun positive_compliant_pcr_test_from_less_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = positivePcrTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun negative_antigenic_test_with_not_allowed_manufacturer_from_less_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))
            .withTestNameRat("random")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun negative_with_not_allowed_type_test_from_less_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))
            .withTestType("unauthorized")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun negative_compliant_pcr_test_from_more_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativePcrTestCertificate(now().minus(27.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun negative_compliant_antigenic_test_from_more_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativeAntigenicTestCertificate(now().minus(27.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun negative_blocklisted_antigenic_test_from_less_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativeAntigenicTestCertificate(now().minus(3.hours))

        givenCountryUvciBlockListContains(testDcc)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("BLOCKLISTED_CERTIFICATE"))
            .body("errors[0].message", equalTo("Certificate is blocklisted"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun negative_blocklisted_pcr_test_from_less_than_24_hours_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))

        givenCountryUvciBlockListContains(testDcc)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[1]"))
            .body("errors[0].code", equalTo("BLOCKLISTED_CERTIFICATE"))
            .body("errors[0].message", equalTo("Certificate is blocklisted"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun compliant_janssen_vaccination_from_less_than_28_days_is_refused() {

        val vaccinationDcc = janssen1_2VaccinationCertificate(today(minus = 25.days))

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun compliant_vaccination_from_more_than_28_days_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 29.days))

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun unauthorized_prophylaxis_vaccination_from_less_than_28_days_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))
            .withVaccinationVaccine("unauthorized")

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun unauthorized_manufacturer_vaccination_from_less_than_28_days_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))
            .withVaccinationManufacturer("unauthorized")

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun unauthorized_medicinal_product_vaccination_from_less_than_28_days_is_refused() {

        val vaccinationDcc = pfizer1_2VaccinationCertificate(today(minus = 25.days))
            .withVaccinationMedicinalProduct("unauthorized")

        val testDcc = negativePcrTestCertificate(now().minus(3.hours))

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(vaccinationDcc),
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(BAD_REQUEST.value())
            .body("error", equalTo(BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }
}
