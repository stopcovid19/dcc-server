package fr.gouv.tac.dcclight.controller

import com.spotify.hamcrest.jackson.IsJsonObject.jsonObject
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.extension.toLocalDate
import fr.gouv.tac.dcclight.test.DccSigningKeysManager.Companion.DCC_SERVER_KEYS
import fr.gouv.tac.dcclight.test.IntegrationTest
import fr.gouv.tac.dcclight.test.NettyManager.Companion.givenCountryUvciBlockListContains
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.When
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.bodyEncrypted
import fr.gouv.tac.dcclight.test.RestAssuredManager.Companion.givenBaseHeaders
import fr.gouv.tac.dcclight.test.TransportEncryptionKeysManager.Companion.CLIENT_PUBLIC_KEY_BASE64
import fr.gouv.tac.dcclight.test.eu.DccCryptographer.Companion.encodeToDccAndEncrypt
import fr.gouv.tac.dcclight.test.extension.withTestCountry
import fr.gouv.tac.dcclight.test.extension.withTestFacility
import fr.gouv.tac.dcclight.test.extension.withTestIdentifier
import fr.gouv.tac.dcclight.test.extension.withTestIssuer
import fr.gouv.tac.dcclight.test.extension.withTestNameRat
import fr.gouv.tac.dcclight.test.extension.withTestResult
import fr.gouv.tac.dcclight.test.extension.withTestTarget
import fr.gouv.tac.dcclight.test.extension.withTestType
import fr.gouv.tac.dcclight.test.matchers.isEuDcc
import fr.gouv.tac.dcclight.test.matchers.isHcertEncoded
import fr.gouv.tac.dcclight.test.matchers.matchesUvciPattern
import fr.gouv.tac.dcclight.test.matchers.where
import fr.gouv.tac.dcclight.test.positiveAntigenicTestCertificate
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.atStartOfDayIn
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.OK
import kotlin.time.Duration.Companion.days

@IntegrationTest
class ConversionFromPositiveAntigenicTestToRecoveryDccTest {

    @ParameterizedTest
    @CsvSource(value = ["random, DGSAG", "TV, TV-DGSAG"])
    fun positive_antigenic_test_input_match_conditions_then_convert_to_recovery_dcc(
        testFacility: String,
        expectedCertificateIssuer: String
    ) {

        val testDate = now().minus(60.days)

        val testDcc = positiveAntigenicTestCertificate(testDate)
            .withTestTarget("840539006")
            .withTestIssuer("APHP")
            .withTestNameRat("1833")
            .withTestFacility(testFacility)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(OK.value())
            .bodyEncrypted(
                "response",
                jsonObject()
                    .where(
                        "certificate",
                        isHcertEncoded(
                            issuedBy = DCC_SERVER_KEYS,
                            euDcc = isEuDcc()
                                .forJeromeDupont()
                                .withProofType("r")
                                .where("du", testDate.plus(180.days).toLocalDate())
                                .where("co", "FR")
                                .where("ci", matchesUvciPattern())
                                .where("is", expectedCertificateIssuer)
                                .where("tg", "840539006")
                                .where("fr", testDate.toLocalDate())
                                .where("df", testDate.plus(11.days).toLocalDate())
                        )
                    )
            )
    }

    @Test
    fun conversion_fail_when_test_certificate_has_blocklisted_country_identifier_hash() {

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days))

        givenCountryUvciBlockListContains(testDcc)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[0]"))
            .body("errors[0].code", equalTo("BLOCKLISTED_CERTIFICATE"))
            .body("errors[0].message", equalTo("Certificate is blocklisted"))
            .body("errors.size()", equalTo(1))
    }

    @Test
    fun conversion_fail_when_test_certificate_contains_unsupported_certificateIdentifier_prefix() {

        val testDcc = positiveAntigenicTestCertificate(now().minus(60.days))
            .withTestIdentifier("URN:UVCI:01:FR:DGSAG/test")

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates[0]"))
            .body("errors[0].code", equalTo("AGGREGATE_CERTIFICATE_NOT_ALLOWED"))
            .body("errors[0].message", equalTo("Certificate comes from a previous aggregation"))
            .body("errors.size()", equalTo(1))
    }

    @ParameterizedTest
    @CsvSource(
        value = [
            "84053900,  LP217198-3, 1232, 2022-01-01, 260373001, FR, APHP",
            "840539006, LP217198,   1232, 2022-01-01, 260373001, FR, APHP",
            "840539006, LP217198-3, 1a,   2022-01-01, 260373001, FR, APHP",
            "840539006, LP217198-3, 1232, 2021-01-01, 260373001, FR, APHP",
            "840539006, LP217198-3, 1232, 2022-01-01, 26037300,  FR, APHP",
            "840539006, LP217198-3, 1232, 2022-01-01, 260373001, DE, APHP",
            "840539006, LP217198-3, 1232, 2022-01-01, 260373001, FR, AP",
        ]
    )
    fun positive_antigenic_test_input_does_not_match_when_values(
        target: String,
        type: String,
        manufacturer: String,
        dateTimeSample: String,
        resultPositive: String,
        country: String,
        certificateIssuer: String
    ) {

        val date = LocalDate.parse(dateTimeSample).atStartOfDayIn(TimeZone.currentSystemDefault())

        val testDcc = positiveAntigenicTestCertificate(date)
            .withTestCountry(country)
            .withTestIssuer(certificateIssuer)
            .withTestTarget(target)
            .withTestType(type)
            .withTestNameRat(manufacturer)
            .withTestResult(resultPositive)

        givenBaseHeaders()
            .body(
                DccAggregationRequest(
                    publicKey = CLIENT_PUBLIC_KEY_BASE64,
                    certificates = listOf(
                        encodeToDccAndEncrypt(testDcc)
                    ),
                )
            )

            .When()
            .post("/api/v1/aggregate")

            .then()
            .statusCode(HttpStatus.BAD_REQUEST.value())
            .body("error", equalTo(HttpStatus.BAD_REQUEST.reasonPhrase))
            .body("message", equalTo("Request body contains invalid attributes"))
            .body("errors[0].field", equalTo("certificates"))
            .body("errors[0].code", equalTo("INCOMPATIBLE_DCC_COMBINATION"))
            .body("errors[0].message", equalTo("Provided certificates are not compatible with any operation"))
            .body("errors.size()", equalTo(1))
    }
}
