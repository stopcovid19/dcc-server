package fr.gouv.tac.dcclight.rules.model

import ehn.techiop.hcert.kotlin.data.GreenCertificate

class Rules(val rules: List<Rule> = listOf()) {

    fun getCorrespondingRule(temporallySortedDcc: List<GreenCertificate>): Rule? =
        rules.filter { it.inputRules.size == temporallySortedDcc.size }
            .find { it.matches(temporallySortedDcc) }

    fun getRulesNames() = rules.map { it.id }
}
