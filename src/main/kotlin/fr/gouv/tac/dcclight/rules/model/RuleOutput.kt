package fr.gouv.tac.dcclight.rules.model

import fr.gouv.tac.dcclight.extension.instant
import fr.gouv.tac.dcclight.extension.instantOrNull
import fr.gouv.tac.dcclight.extension.isInstant
import fr.gouv.tac.dcclight.rules.SanitaryProofType
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.jsonPrimitive
import kotlin.time.Duration.Companion.days

data class RuleOutput(
    val schema: DccSchema,
    val type: SanitaryProofType,
    val referentialDccRanking: Int,
    val override: List<AttributeOverrideSpec>,
    val copy: List<AttributeCopySpec>,
    val increment: List<AttributeIncrementSpec>
) {

    fun getAttributesToOverride() = override.associate { it.bigram to it.value }

    fun getAttributesToCopyFrom(sourceProof: JsonObject) =
        copy
            // find value to copy in sourceProof
            .associate { it.bigramTarget to sourceProof[it.bigramSource]?.jsonPrimitive }
            // if attribute value is an instant, reformat as a complete ISO 8601 datetime
            .mapValues {
                it.value?.jsonPrimitive?.instantOrNull()?.toString() ?: it.value?.jsonPrimitive?.contentOrNull
            }
            // remove elements which didn't exist in sourceProof
            .filter { it.value != null }
            .mapValues { it.value!! }

    fun incrementInstantValue(attribute: String, value: JsonPrimitive): JsonPrimitive {
        val incrementAction = increment.firstOrNull { it.bigram == attribute }
        return if (incrementAction != null && value.isInstant()) {
            val newInstantValue = value.instant().plus(incrementAction.daysToAdd.days)
            JsonPrimitive(newInstantValue.toString())
        } else {
            value
        }
    }
}

data class AttributeOverrideSpec(
    val bigram: String,
    val value: String
)

data class AttributeCopySpec(
    val bigramTarget: String,
    val bigramSource: String
)

data class AttributeIncrementSpec(
    val bigram: String,
    val daysToAdd: Int
)
