package fr.gouv.tac.dcclight.rules.model

import fr.gouv.tac.dcclight.extension.toLocalDate
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.Instant
import kotlinx.datetime.Instant.Companion.DISTANT_FUTURE
import kotlinx.datetime.Instant.Companion.DISTANT_PAST
import kotlinx.datetime.LocalDate
import kotlin.time.Duration
import kotlin.time.Duration.Companion.INFINITE

class DateConstraints(
    validIfAfter: LocalDate?,
    validIfBefore: LocalDate?,
    validIfDatedWithin: Duration?,
    minDaysAfterPreviousDcc: Int?
) {
    private val validIfAfter: LocalDate = validIfAfter ?: DISTANT_PAST.toLocalDate()
    private val validIfBefore: LocalDate = validIfBefore ?: DISTANT_FUTURE.toLocalDate()
    private val validIfDatedWithin = validIfDatedWithin ?: INFINITE
    val minDaysAfterPreviousDcc = minDaysAfterPreviousDcc ?: 0

    constructor() : this(null, null, null, null)

    fun matches(proofInstant: Instant) = validIfAfter.rangeTo(validIfBefore).contains(proofInstant.toLocalDate()) &&
        now().minus(validIfDatedWithin) < proofInstant
}
