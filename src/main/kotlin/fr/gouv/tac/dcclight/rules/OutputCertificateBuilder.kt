package fr.gouv.tac.dcclight.rules

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getProofAsJsonObject
import fr.gouv.tac.dcclight.extension.getProofType
import fr.gouv.tac.dcclight.extension.mapJsonPrimitiveValues
import fr.gouv.tac.dcclight.extension.plus
import fr.gouv.tac.dcclight.extension.trimTransliteratedNames
import fr.gouv.tac.dcclight.extension.withRecoveryStatement
import fr.gouv.tac.dcclight.extension.withTest
import fr.gouv.tac.dcclight.extension.withVaccination
import fr.gouv.tac.dcclight.rules.SanitaryProofType.RECOVERY_STATEMENT
import fr.gouv.tac.dcclight.rules.SanitaryProofType.TEST
import fr.gouv.tac.dcclight.rules.SanitaryProofType.VACCINATION
import fr.gouv.tac.dcclight.rules.model.RuleOutput
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonPrimitive

class OutputCertificateBuilder(
    private val referenceCertificate: GreenCertificate,
    private val outputRule: RuleOutput,
    private val certificateIdentifier: String
) {

    fun build(): GreenCertificate {
        val inputSanitaryProof = referenceCertificate.getProofAsJsonObject()

        val outputProof = JsonObject(emptyMap())
            .plus(getInitialValues(inputSanitaryProof, referenceCertificate.getProofType()))
            .plus(outputRule.getAttributesToOverride())
            .plus(outputRule.getAttributesToCopyFrom(inputSanitaryProof))
            .mapJsonPrimitiveValues { outputRule.incrementInstantValue(it.key, it.value) }
            .plus("ci" to certificateIdentifier)

        val baseOutputGreenCertificate = GreenCertificate(
            schemaVersion = "1.3.0",
            subject = referenceCertificate.subject.trimTransliteratedNames(),
            dateOfBirthString = referenceCertificate.dateOfBirthString,
        )
        return when (outputRule.type) {
            TEST -> baseOutputGreenCertificate.withTest(outputProof)
            RECOVERY_STATEMENT -> baseOutputGreenCertificate.withRecoveryStatement(outputProof)
            VACCINATION -> baseOutputGreenCertificate.withVaccination(outputProof)
        }
    }

    private fun getInitialValues(sourceProof: JsonObject, sourceProofType: SanitaryProofType): Map<String, String> =
        if (sourceProofType == outputRule.type) {
            sourceProof
                .filterValues { it is JsonPrimitive }
                .mapValues { it.value.jsonPrimitive.content }
        } else {
            emptyMap()
        }
}
