package fr.gouv.tac.dcclight.extension

import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonPrimitive
import java.util.Map.entry

/**
 * Adds or override an attribute to the current JsonObject.
 * @param attribute a key-value pair attribute to add
 */
internal fun JsonObject.plus(attribute: Pair<String, String>): JsonObject {
    val jsonAttribute = attribute.first to JsonPrimitive(attribute.second)
    return JsonObject(this.plus(jsonAttribute))
}

/**
 * Adds or override attributes to the current JsonObject.
 * @param attributes a map of attributes to add or replace
 */
internal fun JsonObject.plus(attributes: Map<String, String>): JsonObject {
    val jsonAttributes = attributes.mapValues { JsonPrimitive(it.value) }
    return JsonObject(this.plus(jsonAttributes))
}

/**
 * Adds or override an attribute to the current JsonObject only if the second value is non-null.
 * @param attribute a key-value pair attribute to add
 */
internal fun JsonObject.plusOptional(attribute: Pair<String, String?>): JsonObject {
    val name = attribute.first
    val value = attribute.second
    return if (value == null) this else this.plus(name to value)
}

/**
 * Transforms the JsonPrimitive values of the current JsonObject using given transformation function.
 */
internal fun JsonObject.mapJsonPrimitiveValues(transform: (Map.Entry<String, JsonPrimitive>) -> JsonPrimitive): JsonObject {
    val transformedAttributes = this.mapValues {
        when (it.value) {
            is JsonPrimitive -> transform(entry(it.key, it.value.jsonPrimitive))
            else -> it.value
        }
    }
    return JsonObject(transformedAttributes)
}
