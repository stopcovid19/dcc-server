package fr.gouv.tac.dcclight.extension

import kotlinx.datetime.toKotlinInstant

fun java.util.Date.toKLocalDate() = this.toInstant().toKotlinInstant().toLocalDate()
