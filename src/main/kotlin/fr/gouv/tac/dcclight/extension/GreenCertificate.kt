package fr.gouv.tac.dcclight.extension

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.rules.SanitaryProofType.RECOVERY_STATEMENT
import fr.gouv.tac.dcclight.rules.SanitaryProofType.TEST
import fr.gouv.tac.dcclight.rules.SanitaryProofType.VACCINATION
import kotlinx.datetime.TimeZone
import kotlinx.datetime.atStartOfDayIn
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.encodeToJsonElement
import kotlinx.serialization.json.jsonObject

fun GreenCertificate.getCountry() =
    when (this.getProofType()) {
        VACCINATION -> this.vaccinations!![0]!!.country
        TEST -> this.tests!![0]!!.country
        RECOVERY_STATEMENT -> this.recoveryStatements!![0]!!.country
    }

fun GreenCertificate.getIssuer() =
    when (this.getProofType()) {
        VACCINATION -> this.vaccinations!![0]!!.certificateIssuer
        TEST -> this.tests!![0]!!.certificateIssuer
        RECOVERY_STATEMENT -> this.recoveryStatements!![0]!!.certificateIssuer
    }

fun GreenCertificate.getIdentifier() =
    when (this.getProofType()) {
        VACCINATION -> this.vaccinations!![0]!!.certificateIdentifier
        TEST -> this.tests!![0]!!.certificateIdentifier
        RECOVERY_STATEMENT -> this.recoveryStatements!![0]!!.certificateIdentifier
    }

/**
 * Returns either dateTime if Test, either start of day if Vaccination or Recovery statement
 */
fun GreenCertificate.getProofInstant() =
    when (this.getProofType()) {
        VACCINATION -> this.vaccinations!![0]!!.date.atStartOfDayIn(TimeZone.currentSystemDefault())
        TEST -> this.tests!![0]!!.dateTimeSample
        RECOVERY_STATEMENT -> this.recoveryStatements!![0]!!.dateOfFirstPositiveTestResult.atStartOfDayIn(TimeZone.currentSystemDefault())
    }

fun GreenCertificate.getProofType() =
    when {
        this.hasOneTest() -> TEST
        this.hasOneVaccination() -> VACCINATION
        this.hasOneRecoveryStatement() -> RECOVERY_STATEMENT
        else -> throw IllegalStateException("Trying to compare at least one certificate with no sanitary proof")
    }

fun GreenCertificate.getProofAsJsonObject() =
    when (this.getProofType()) {
        TEST -> Json.encodeToJsonElement(this.getTest())
        VACCINATION -> Json.encodeToJsonElement(this.getVaccination())
        RECOVERY_STATEMENT -> Json.encodeToJsonElement(this.getRecoveryStatement())
    }
        .jsonObject

fun GreenCertificate.hasOneTest(): Boolean = (this.tests?.size ?: 0) == 1
fun GreenCertificate.hasOneVaccination(): Boolean = (this.vaccinations?.size ?: 0) == 1
fun GreenCertificate.hasOneRecoveryStatement(): Boolean = (this.recoveryStatements?.size ?: 0) == 1

fun GreenCertificate.getRecoveryStatement() =
    (this.recoveryStatements ?: throw IllegalStateException())[0] ?: throw IllegalStateException()

fun GreenCertificate.getTest() = (this.tests ?: throw IllegalStateException())[0] ?: throw IllegalStateException()
fun GreenCertificate.getVaccination() =
    (this.vaccinations ?: throw IllegalStateException())[0] ?: throw IllegalStateException()

fun GreenCertificate.withTest(test: JsonObject) =
    this.copy(tests = arrayOf(Json.decodeFromJsonElement(test)))

fun GreenCertificate.withVaccination(vaccination: JsonObject) =
    this.copy(vaccinations = arrayOf(Json.decodeFromJsonElement(vaccination)))

fun GreenCertificate.withRecoveryStatement(recoveryStatement: JsonObject) =
    this.copy(recoveryStatements = arrayOf(Json.decodeFromJsonElement(recoveryStatement)))

fun GreenCertificate.countryUvciHash(): String = "${this.getCountry()}${this.getIdentifier()}".toByteArray().sha256().take(32)
fun GreenCertificate.uvciHash(): String = this.getIdentifier().toByteArray().sha256().take(32)
