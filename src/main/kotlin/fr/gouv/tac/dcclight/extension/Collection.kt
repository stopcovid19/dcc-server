package fr.gouv.tac.dcclight.extension

fun <T> Collection<T>.isEmptyOrContains(element: T) = this.isEmpty() || this.contains(element)
