package fr.gouv.tac.dcclight.extension

import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.atStartOfDayIn
import kotlinx.serialization.json.JsonPrimitive

/**
 * Returns content of the current element as an Instant.
 */
internal fun JsonPrimitive.isInstant() = this.instantOrNull() != null

/**
 * Returns content of the current element as an Instant.
 */
internal fun JsonPrimitive.instant() = try {
    Instant.parse(this.content)
} catch (ex: Exception) {
    LocalDate.parse(this.content).atStartOfDayIn(TimeZone.currentSystemDefault())
}

/**
 * Returns content of the current element as an Instant or null if current element is not a valid representation of an ISO 8601 date/time.
 */
internal fun JsonPrimitive.instantOrNull() = try {
    this.instant()
} catch (ex: Exception) {
    null
}
