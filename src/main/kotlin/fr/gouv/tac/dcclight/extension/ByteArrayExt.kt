/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package fr.gouv.tac.dcclight.extension

import org.apache.commons.codec.binary.Hex
import java.security.MessageDigest
import java.security.SecureRandom

fun ByteArray.randomize() {
    for (i in 0 until size) {
        set(i, SecureRandom().nextInt().toByte())
    }
}

fun <T> ByteArray.use(block: (ByteArray) -> T): T {
    val res = block(this)
    this.randomize()
    return res
}

fun ByteArray.sha256(): String = Hex.encodeHexString(
    MessageDigest.getInstance("SHA-256")
        .digest(this)
)
