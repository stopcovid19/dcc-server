package fr.gouv.tac.dcclight.controller

import com.fasterxml.jackson.databind.ObjectMapper
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.api.AggregateApi
import fr.gouv.tac.dcclight.api.model.CertificatesAggregate
import fr.gouv.tac.dcclight.api.model.DccAggregationRequest
import fr.gouv.tac.dcclight.api.model.EncryptedApiResponse
import fr.gouv.tac.dcclight.extension.getProofInstant
import fr.gouv.tac.dcclight.service.DccDecodingService
import fr.gouv.tac.dcclight.service.RulesEngineService
import fr.gouv.tac.dcclight.service.TransportLayerEncryptionService
import fr.gouv.tac.dcclight.service.error.DccValidationError.MAX_NUMBER_OF_CERTIFICATES_EXCEEDED
import fr.gouv.tac.dcclight.service.error.rejectCertificates
import fr.gouv.tac.dcclight.service.error.rejectPublicKey
import org.springframework.http.HttpStatus.OK
import org.springframework.http.ResponseEntity
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.BindException
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.spec.InvalidKeySpecException
import java.util.Base64
import javax.crypto.AEADBadTagException
import kotlin.text.Charsets.UTF_8

@RestController
@RequestMapping(path = ["/api/v1"])
class AggregateController(
    val objectMapper: ObjectMapper,
    val dccDecodingService: DccDecodingService,
    val rulesEngineService: RulesEngineService,
    val encryptionService: TransportLayerEncryptionService
) : AggregateApi {

    override fun aggregate(dccAggregationRequest: DccAggregationRequest): ResponseEntity<EncryptedApiResponse> {

        val validation = BeanPropertyBindingResult(dccAggregationRequest, "aggregationRequest")

        if (dccAggregationRequest.certificates.size > rulesEngineService.getMaxItems()) {
            validation.rejectCertificates(MAX_NUMBER_OF_CERTIFICATES_EXCEEDED)
            throw BindException(validation)
        }

        val key = try {
            encryptionService.getEncryptionKey(Base64.getDecoder().decode(dccAggregationRequest.publicKey))
        } catch (e: Throwable) {
            when (e) {
                is InvalidKeySpecException, is IllegalArgumentException -> {
                    validation.rejectPublicKey()
                    throw BindException(validation)
                }
                else -> throw e
            }
        }

        val decryptedCertificates = dccAggregationRequest.certificates
            .filter { it != null }
            .map {
                // B64
                val base64DecodedCertificate = Base64.getDecoder().decode(it)
                // software decryption
                try {
                    encryptionService.decrypt(key, base64DecodedCertificate).toString(UTF_8)
                } catch (e: AEADBadTagException) {
                    validation.rejectPublicKey()
                    throw BindException(validation)
                }
            }
        // EU lib decode and verification
        val decodedCertificates = dccDecodingService.decodeAndValidate(key, decryptedCertificates, validation)
            .sortedBy(GreenCertificate::getProofInstant)

        val encodedAggregateCertificate = rulesEngineService.build(decodedCertificates, validation)

        // software encryption
        val encryptedAggregateCertificate = encryptionService.encrypt(
            key,
            objectMapper.writeValueAsString(CertificatesAggregate(encodedAggregateCertificate)).toByteArray()
        )
        // B64
        val base64EncodedAggregateCertificate = Base64.getEncoder().encodeToString(encryptedAggregateCertificate)

        return ResponseEntity(EncryptedApiResponse(base64EncodedAggregateCertificate), OK)
    }
}
