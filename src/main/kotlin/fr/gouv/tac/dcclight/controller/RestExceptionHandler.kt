package fr.gouv.tac.dcclight.controller

import com.fasterxml.jackson.core.JacksonException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import ehn.techiop.hcert.kotlin.chain.VerificationException
import fr.gouv.tac.dcclight.api.model.ErrorResponse
import fr.gouv.tac.dcclight.api.model.ErrorResponseErrorsInner
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.stereotype.Controller
import org.springframework.validation.BindException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.OffsetDateTime.now
import javax.servlet.http.HttpServletRequest
import javax.validation.ConstraintViolationException

@RestControllerAdvice(annotations = [Controller::class])
class RestExceptionHandler(private val servletRequest: HttpServletRequest) : ResponseEntityExceptionHandler() {

    private val log = LoggerFactory.getLogger(RestExceptionHandler::class.java)

    override fun handleHttpMessageNotReadable(
        ex: HttpMessageNotReadableException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val cause = ex.cause
        val rootCause = ex.rootCause
        val field = if (cause is MismatchedInputException) {
            cause.path
                .map { if (it.fieldName != null) ".${it.fieldName}" else "[${it.index}]" }
                .joinToString("")
                .removePrefix(".")
        } else {
            ""
        }
        return badRequestAndLogErrors(
            listOf(
                ErrorResponseErrorsInner(
                    field = field,
                    code = "HttpMessageNotReadable",
                    message = if (rootCause is JacksonException) rootCause.originalMessage else rootCause?.message
                )
            )
        )
    }

    override fun handleBindException(
        ex: BindException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        return badRequestAndLogErrors(
            ex.fieldErrors.map {
                ErrorResponseErrorsInner(
                    it.field,
                    it.code,
                    it.defaultMessage
                )
            } + ex.globalErrors.map {
                ErrorResponseErrorsInner(
                    "",
                    it.code,
                    it.defaultMessage
                )
            }
        )
    }

    @ExceptionHandler
    fun handle(
        ex: ConstraintViolationException,
        request: HttpServletRequest
    ): ResponseEntity<Any> {

        return badRequestAndLogErrors(
            ex.constraintViolations.map {
                ErrorResponseErrorsInner(
                    field = it.propertyPath.toString(),
                    code = it.constraintDescriptor.annotation.annotationClass.simpleName,
                    message = it.message
                )
            }
        )
    }

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        return badRequestAndLogErrors(
            ex.fieldErrors.map {
                ErrorResponseErrorsInner(
                    it.field,
                    it.code,
                    it.defaultMessage
                )
            } + ex.globalErrors.map {
                ErrorResponseErrorsInner(
                    "",
                    it.code,
                    it.defaultMessage
                )
            }
        )
    }

    @ExceptionHandler
    fun handle(
        ex: VerificationException,
        request: HttpServletRequest
    ): ResponseEntity<ErrorResponse> {
        val errorResponseBody = ErrorResponse(
            status = BAD_REQUEST.value(),
            error = BAD_REQUEST.reasonPhrase,
            message = "DCC Decoding error: ${ex.message}",
            timestamp = now(),
            path = servletRequest.requestURI,
        )
        return ResponseEntity(errorResponseBody, BAD_REQUEST)
    }

    override fun handleExceptionInternal(
        ex: Exception,
        body: Any?,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        super.handleExceptionInternal(ex, body, headers, status, request)
        val errorResponseBody = ErrorResponse(
            status = status.value(),
            error = status.reasonPhrase,
            message = ex.message ?: "Internal error",
            timestamp = now(),
            path = servletRequest.requestURI
        )

        if (status.is5xxServerError) {
            log.error(ex.message)
        } else {
            log.warn(ex.message)
        }

        return ResponseEntity(errorResponseBody, status)
    }

    private fun badRequestAndLogErrors(errors: List<ErrorResponseErrorsInner>): ResponseEntity<Any> {
        val errorResponseBody = ErrorResponse(
            status = BAD_REQUEST.value(),
            error = BAD_REQUEST.reasonPhrase,
            message = "Request body contains invalid attributes",
            timestamp = now(),
            path = servletRequest.requestURI,
            errors = errors
        )
        errorResponseBody.errors?.forEach {
            log.info(
                "Validation error on {} {}: '{}' {}",
                servletRequest.method,
                servletRequest.requestURI,
                it.field,
                it.message
            )
        }
        return ResponseEntity(errorResponseBody, BAD_REQUEST)
    }
}
