package fr.gouv.tac.dcclight.model

import com.fasterxml.jackson.annotation.JsonProperty

data class DccBlocklist(
    @JsonProperty("signature_hashes")
    val signatureHashes: Set<String>,
    @JsonProperty("uvci_hashes")
    val uvciHashes: Set<String>,
    @JsonProperty("country_uvci_hashes")
    val countryUvciHashes: Set<String>
)
