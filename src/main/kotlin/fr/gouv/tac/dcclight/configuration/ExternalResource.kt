package fr.gouv.tac.dcclight.configuration

import io.micrometer.core.instrument.Gauge
import io.micrometer.core.instrument.MeterRegistry
import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.Instant
import org.slf4j.LoggerFactory
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.scheduling.TaskScheduler
import org.springframework.scheduling.support.CronTrigger
import org.springframework.web.reactive.function.client.WebClient
import java.io.InputStream
import javax.annotation.PostConstruct

class ExternalResource<T>(
    private val endpointConfiguration: Endpoint,
    initialValue: T,
    metricName: String,
    meterRegistry: MeterRegistry,
    taskScheduler: TaskScheduler,
    private val webClient: WebClient,
    private val dataExtractor: (InputStream) -> T
) {

    private val log = LoggerFactory.getLogger(this::class.java)

    var content: T = initialValue
        private set

    private var lastRenewal = Instant.fromEpochSeconds(0)

    init {
        Gauge.builder("dcc.configuration.age") { now().minus(lastRenewal).inWholeSeconds }
            .description("Time elapsed since last configuration renewal")
            .tag("config", metricName)
            .tag("url", endpointConfiguration.url.toString())
            .baseUnit("seconds")
            .register(meterRegistry)
        taskScheduler.schedule(this::fetch, CronTrigger(endpointConfiguration.refreshScheduling.cron))
    }

    @PostConstruct
    internal fun fetch() {
        try {
            lastRenewal = now()
            content = webClient.get()
                .uri(endpointConfiguration.url.toURI())
                .headers { optionalBearerAuth(it) }
                .retrieve()
                .bodyToMono(InputStreamResource::class.java)
                .map { it.inputStream.use(dataExtractor) }
                .block()!!
        } catch (e: Throwable) {
            log.warn("Error fetching configuration at ${endpointConfiguration.url}, the app will use previous data", e)
        }
    }

    private fun optionalBearerAuth(headers: HttpHeaders) {
        endpointConfiguration.token?.let {
            headers.setBearerAuth(it)
        }
    }
}
