package fr.gouv.tac.dcclight.configuration

import fr.gouv.tac.dcclight.rules.model.yamlRulesObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class DccServerConfiguration {

    @Bean
    fun webClient(builder: WebClient.Builder) = builder.build()

    @Bean
    fun yamlObjectMapper() = yamlRulesObjectMapper()
}
