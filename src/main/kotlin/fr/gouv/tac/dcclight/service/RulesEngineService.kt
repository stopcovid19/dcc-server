package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.configuration.ExternalResource
import fr.gouv.tac.dcclight.extension.getIdentifier
import fr.gouv.tac.dcclight.extension.getProofInstant
import fr.gouv.tac.dcclight.extension.trimTransliteratedNames
import fr.gouv.tac.dcclight.rules.OutputCertificateBuilder
import fr.gouv.tac.dcclight.rules.model.DccSchema.DCC
import fr.gouv.tac.dcclight.rules.model.DccSchema.DCCLIGHT
import fr.gouv.tac.dcclight.rules.model.Rules
import fr.gouv.tac.dcclight.service.dcc.CertificatesAggregateIdentifierBuilder
import fr.gouv.tac.dcclight.service.error.DccValidationError.INCOMPATIBLE_DCC_COMBINATION
import fr.gouv.tac.dcclight.service.error.rejectCertificates
import org.springframework.stereotype.Service
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult

@Service
class RulesEngineService(
    private val aggregationRules: ExternalResource<Rules>,
    private val metricsService: MetricsService,
    private val dcclightEncodingService: DccLightEncodingService,
    private val dccEncodingService: DccEncodingService
) {

    fun build(decodedCertificates: List<GreenCertificate>, validation: BindingResult): String {
        val aggregationRule = findMatchingRuleOrReject(decodedCertificates, validation)
        val outputRule = aggregationRule.output

        val referentialDcc = decodedCertificates[outputRule.referentialDccRanking - 1]

        return when (outputRule.schema) {
            DCC -> dccEncodingService.encode(
                OutputCertificateBuilder(
                    referenceCertificate = referentialDcc,
                    outputRule = outputRule,
                    certificateIdentifier = computeCertificateIdentifier(decodedCertificates)
                ).build()
            )
            DCCLIGHT -> dcclightEncodingService.encode(
                GreenCertificate(
                    "1.0.0",
                    referentialDcc.subject.trimTransliteratedNames(),
                    referentialDcc.dateOfBirthString
                ),
                referentialDcc.getProofInstant()
            )
        }
            .also { metricsService.countAggregationSuccessfullyGenerated(aggregationRule.id) }
    }

    fun getMaxItems(): Int = aggregationRules.content.rules.maxOf { r -> r.inputRules.size }

    private fun findMatchingRuleOrReject(
        decodedCertificates: List<GreenCertificate>,
        validation: BindingResult
    ) = aggregationRules.content.getCorrespondingRule(decodedCertificates) ?: run {
        validation.rejectCertificates(INCOMPATIBLE_DCC_COMBINATION)
        metricsService.incrementCounter(INCOMPATIBLE_DCC_COMBINATION)
        metricsService.logRejectDccWithIncompatibleDccCombination(decodedCertificates)
        throw BindException(validation)
    }

    private fun computeCertificateIdentifier(decodedCertificates: List<GreenCertificate>) =
        CertificatesAggregateIdentifierBuilder(decodedCertificates.map { it.getIdentifier() }).build()
}
