package fr.gouv.tac.dcclight.service.dcc

import COSE.AlgorithmID
import COSE.HeaderKeys
import com.upokecenter.cbor.CBORObject
import ehn.techiop.hcert.kotlin.trust.CoseAdapter
import fr.gouv.tac.dcclight.extension.sha256

/**
 * See EU documentation : https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32022D0483&qid=1669713394580&from=FR
 * chapter 9.4
 */
class DccSignatureHash(private val cose: ByteArray) {

    fun buildSignatureHash(): String {

        val signature = getSignature(cose)

        return when (val alg = getSignatureAlgorithmID(cose)) {
            AlgorithmID.RSA_PSS_256, AlgorithmID.RSA_PSS_512, AlgorithmID.RSA_PSS_384 -> signature.sha256()
                .take(32)

            AlgorithmID.ECDSA_256, AlgorithmID.ECDSA_384, AlgorithmID.ECDSA_512 -> getRValueFromSignature(signature).sha256()
                .take(32)

            else -> throw RuntimeException("Unsupported signature algorithm $alg")
        }
    }

    private fun getRValueFromSignature(signature: ByteArray): ByteArray {
        return signature.copyOfRange(0, signature.size / 2)
    }

    private fun getSignature(cose: ByteArray): ByteArray {
        val messageObject = CBORObject.DecodeFromBytes(augmentedInput(cose))
        return messageObject.get(3).GetByteString()
    }

    private fun getSignatureAlgorithmID(cose: ByteArray): AlgorithmID {
        val coseAdapter = CoseAdapter(cose)
        return AlgorithmID.FromCBOR(coseAdapter.sign1Message.findAttribute(HeaderKeys.Algorithm))
    }

    /**
     * Input may be missing COSE Tag 0xD2 = 18 = cose-sign1.
     * But we need this, to cast the parsed object to [Cbor.Tagged].
     * So we'll add the Tag to the input.
     *
     * It may also be tagged as a CWT (0xD8, 0x3D) and a Sign1 (0xD2).
     * But the library expects only one tag.
     * So we'll strip the CWT tag from the input.
     */
    private fun augmentedInput(input: ByteArray): ByteArray {
        return if (input.isNotEmpty() && isArray(input[0]))
            byteArrayOf(0xD2.toByte()) + input
        else if (input.size >= 3 && isCwt(input[0], input[1]) && isSign1(input[2])) {
            input.drop(2).toByteArray()
        } else {
            input
        }
    }

    private fun isSign1(byte: Byte) = byte == 0xD2.toByte()
    private fun isCwt(firstByte: Byte, secondByte: Byte) = firstByte == 0xD8.toByte() && secondByte == 0x3D.toByte()
    private fun isArray(byte: Byte) = byte == 0x84.toByte()
}
