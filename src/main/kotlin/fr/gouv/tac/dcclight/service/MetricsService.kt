package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.chain.DecodeResult
import ehn.techiop.hcert.kotlin.chain.Error
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.configuration.ExternalResource
import fr.gouv.tac.dcclight.extension.getProofInstant
import fr.gouv.tac.dcclight.extension.getProofType
import fr.gouv.tac.dcclight.extension.getRecoveryStatement
import fr.gouv.tac.dcclight.extension.getTest
import fr.gouv.tac.dcclight.extension.getVaccination
import fr.gouv.tac.dcclight.rules.SanitaryProofType.RECOVERY_STATEMENT
import fr.gouv.tac.dcclight.rules.SanitaryProofType.TEST
import fr.gouv.tac.dcclight.rules.SanitaryProofType.VACCINATION
import fr.gouv.tac.dcclight.rules.model.Rules
import fr.gouv.tac.dcclight.service.error.DccValidationError
import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import kotlinx.datetime.Clock.System.now
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders.USER_AGENT
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest

@Service
class MetricsService(
    private val meterRegistry: MeterRegistry,
    private val servletRequest: HttpServletRequest,
    private val aggregationRules: ExternalResource<Rules>
) {

    private val log = LoggerFactory.getLogger(this::class.java)

    private val rulesCounters: Map<RequestSource, MutableMap<String, Counter>> =
        RequestSource.values().associateWith { mutableMapOf() }

    private val functionalErrorCounters = RequestSource.values().associateWith { requestSource ->
        DccValidationError.values().associateWith { validationError ->
            Counter.builder("dcc.request.aggregation.error")
                .description(validationError.message)
                .tag("type", validationError.name)
                .tag("source", requestSource.toString())
                .register(meterRegistry)
        }
    }

    private val hcertErrorCounters = RequestSource.values().associateWith { requestSource ->
        Error.values().associateWith { hcertError ->
            Counter.builder("dcc.request.hcert.error")
                .description(hcertError.name)
                .tag("type", hcertError.name)
                .tag("source", requestSource.toString())
                .register(meterRegistry)
        }
    }

    private val flexIdAccentsMismatchs = Counter.builder("dcc.request.flexId.accents.mismatch")
        .description("Number of error due to accented characters")
        .tag("reason", "accents")
        .register(meterRegistry)

    @Scheduled(cron = "0 0 0 * * *")
    fun cleanObsoleteCounters() {
        val actualRulesNames = aggregationRules.content.getRulesNames()
        rulesCounters.values.forEach { counters ->
            counters.entries.removeIf { !actualRulesNames.contains(it.key) }
        }
    }

    fun incrementCounter(error: DccValidationError) {
        val requestSource = getRequestSource()
        val counter = functionalErrorCounters[requestSource]!![error]
        if (counter == null) {
            log.warn("Missing counter for requestSource={} and errorType={}", requestSource, error)
        }
        counter?.increment()
    }

    fun incrementCounter(error: Error) {
        val requestSource = getRequestSource()
        val counter = hcertErrorCounters[requestSource]!![error]
        if (counter == null) {
            log.warn("Missing counter for requestSource={} and errorType={}", requestSource, error)
        }
        counter?.increment()
    }

    fun incrementFlexIdAccentsMismatchs() = flexIdAccentsMismatchs.increment()

    private fun getRequestSource() = RequestSource.parse(servletRequest.getHeader(USER_AGENT))

    fun logRejectDccWithIncompatibleDccCombination(decryptedDCCs: List<GreenCertificate>) {
        log.info(
            """Provided certificates are not compatible with any operation.
               date: ${now()},
               list of DCC: ${decryptedDCCs.mapIndexed { index, it -> getDccInformation(it, index) }}
            """.trimIndent()
        )
    }

    fun logTechnicalValidityDateInThePast(decryptedDCC: DecodeResult) {
        log.info(
            """End of technical validity is in the past.
               DCC type: ${decryptedDCC.chainDecodeResult.eudgc?.getProofType()?.type},
               End of validity: ${decryptedDCC.verificationResult.expirationTime},
               Date: ${now()}
            """.trimIndent()
        )
    }

    fun logRejectDccWithDateInFutureMoreThanOneDay(decryptedDCC: GreenCertificate) {
        log.info(
            """Certificate proof date is in more than 24 hours
               DCC type: ${decryptedDCC.getProofType().type},
               DCC date: ${decryptedDCC.getProofInstant()},
               date: ${now()}
            """.trimIndent()
        )
    }

    fun countAggregationSuccessfullyGenerated(ruleName: String) {
        val requestSource = getRequestSource()
        val counter = rulesCounters[requestSource]!!.getOrPut(ruleName) {
            Counter.builder("dcc.request.aggregation.successful")
                .description("Number of aggregation successfully generated")
                .tag("rule name", ruleName)
                .tag("source", requestSource.toString())
                .register(meterRegistry)
        }
        counter.increment()
    }

    fun getDccInformation(decryptedDCC: GreenCertificate, position: Int): String? {
        when (decryptedDCC.getProofType()) {
            VACCINATION -> {
                val vaccinationProof = decryptedDCC.getVaccination()
                return """
                    {
                        DCC type: v,
                        position: $position,
                        vaccine code: ${vaccinationProof.vaccine.valueSetEntry.display},
                        vaccine type: ${vaccinationProof.medicinalProduct.valueSetEntry.display},
                        vaccine manufacturer: ${vaccinationProof.authorizationHolder.valueSetEntry.display},
                        dose number: ${vaccinationProof.doseNumber},
                        total dose number: ${vaccinationProof.doseTotalNumber},
                        country: ${vaccinationProof.country},
                        vaccination date: ${vaccinationProof.date}
                        vaccination certificate issuer: ${vaccinationProof.certificateIssuer}
                    }
                """.trimIndent()
            }
            TEST -> {
                val testProof = decryptedDCC.getTest()
                return """
                    {
                        DCC type: t,
                        position: $position,
                        test type: ${testProof.type.valueSetEntry.display},
                        test name: ${testProof.nameNaa},
                        test device: ${testProof.nameRat?.valueSetEntry?.display},
                        test date sample: ${testProof.dateTimeSample},
                        test date result: ${testProof.dateTimeResult}                    ,
                        test result: ${testProof.resultPositive.valueSetEntry.display},
                        test country: ${testProof.country}
                        test certificate issuer: ${testProof.certificateIssuer}
                    }
                """.trimIndent()
            }
            RECOVERY_STATEMENT -> {
                val recoveryProof = decryptedDCC.getRecoveryStatement()
                return """
                    {
                        DCC type: r,
                        position: $position,
                        recovery date of first positive test: ${recoveryProof.dateOfFirstPositiveTestResult},
                        country: ${recoveryProof.country},
                        recovery certificate valid from: ${recoveryProof.certificateValidFrom},
                        recovery certificate valid until: ${recoveryProof.certificateValidUntil}
                        recovery certificate issuer: ${recoveryProof.certificateIssuer}
                    }
                """.trimIndent()
            }
            else -> throw IllegalStateException("Trying to compare at least one certificate with no sanitary proof")
        }
    }

    enum class RequestSource(private val userAgent: String) {

        PASSPLUSCOVID_V1("PassPlusCovid-V1"),
        DEFAULT("");

        companion object {
            fun parse(userAgent: String): RequestSource = values().find { it.userAgent == userAgent } ?: DEFAULT
        }
    }
}
