package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.chain.fromBase64
import fr.gouv.tac.dcclight.configuration.DccServerProperties
import fr.gouv.tac.dcclight.extension.safeUse
import fr.gouv.tac.dcclight.extension.use
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.security.KeyFactory
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.KeyAgreement
import javax.crypto.Mac
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec

private const val CONVERSION_STRING_INPUT = "conversion"
private const val HASH_HMACSHA256 = "HmacSHA256"
private const val ALGORITHM_ECDH = "ECDH"
private const val ALGORITHM_AES = "AES"

private const val AES_GCM_CIPHER_TYPE = "AES/GCM/NoPadding"
private const val AES_GCM_IV_LENGTH = 12
private const val AES_GCM_TAG_LENGTH_IN_BITS = 128

@Service
class TransportLayerEncryptionService(properties: DccServerProperties) {

    private val decodedServerPrivateKey = properties.encryptionPrivateKey.fromBase64()

    private fun getKeyAgreement(publicKey: ByteArray, privateKey: ByteArray): ByteArray {
        val keyFactory = KeyFactory.getInstance("EC")
        val serverPrivateKey = keyFactory.generatePrivate(PKCS8EncodedKeySpec(privateKey))
        val appPublicKey = keyFactory.generatePublic(X509EncodedKeySpec(publicKey))

        val keyAgreement = KeyAgreement.getInstance(ALGORITHM_ECDH)

        keyAgreement.init(serverPrivateKey)
        keyAgreement.doPhase(appPublicKey, true)

        return keyAgreement.generateSecret().use { sharedSecret ->
            SecretKeySpec(sharedSecret, HASH_HMACSHA256).safeUse { secretKeySpec ->
                val hmac = Mac.getInstance(HASH_HMACSHA256)
                hmac.init(secretKeySpec)
                hmac.doFinal(CONVERSION_STRING_INPUT.toByteArray(Charsets.UTF_8))
            }
        }
    }

    fun getEncryptionKey(appPublicKey: ByteArray): ByteArray {
        return getKeyAgreement(appPublicKey, decodedServerPrivateKey)
    }

    fun decrypt(key: ByteArray, encryptedData: ByteArray): ByteArray {
        val cipher = Cipher.getInstance(AES_GCM_CIPHER_TYPE)
        val iv: ByteArray = encryptedData.copyOfRange(0, AES_GCM_IV_LENGTH)
        val ivSpec = GCMParameterSpec(AES_GCM_TAG_LENGTH_IN_BITS, iv)

        return SecretKeySpec(key, ALGORITHM_AES).safeUse { secretKey ->
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec)
            cipher.doFinal(encryptedData, AES_GCM_IV_LENGTH, encryptedData.size - AES_GCM_IV_LENGTH)
        }
    }

    fun encrypt(key: ByteArray, clearData: ByteArray): ByteArray =
        SecretKeySpec(key, ALGORITHM_AES).safeUse { secretKey ->
            val bos = ByteArrayOutputStream()

            val cipher = Cipher.getInstance(AES_GCM_CIPHER_TYPE)
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
            bos.write(cipher.iv)
            CipherOutputStream(bos, cipher).use { cos -> cos.write(clearData) }

            bos.toByteArray()
        }
}
