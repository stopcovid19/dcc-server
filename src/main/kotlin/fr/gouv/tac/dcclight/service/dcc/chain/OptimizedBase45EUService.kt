package fr.gouv.tac.dcclight.service.dcc.chain

import ehn.techiop.hcert.kotlin.chain.Base45Service
import ehn.techiop.hcert.kotlin.chain.Error
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult

/**
 * Encodes/decodes input in/from Base45
 */
class OptimizedBase45EUService : Base45Service {

    override fun encode(input: ByteArray) =
        input.base45Encode()

    override fun decode(input: String, verificationResult: VerificationResult): ByteArray {
        try {
            return input.base45Decode()
        } catch (e: Throwable) {
            throw VerificationException(Error.BASE_45_DECODING_FAILED, cause = e)
        }
    }
}
