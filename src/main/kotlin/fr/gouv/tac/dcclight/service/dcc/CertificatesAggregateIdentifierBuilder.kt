package fr.gouv.tac.dcclight.service.dcc

import org.apache.commons.codec.digest.DigestUtils
import java.math.BigInteger

private const val CODE_POINTS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/:"
private const val PREFIX = "URN:UVCI:01:FR"
private const val ISSUING_ENTITY = "DGSAG"

class CertificatesAggregateIdentifierBuilder(identifiersList: List<String>) {

    private val concatenatedIdentifiers = identifiersList.joinToString("")

    fun build(): String {
        val bint = BigInteger(1, DigestUtils.sha256(concatenatedIdentifiers))
        val radix = 10 + 26 // letters in the latine alphabet
        val identifier = bint.toString(radix).take(49)
        val prefixIssuingIdentityAndIdentifierToUpperCase = "$PREFIX:$ISSUING_ENTITY/$identifier".uppercase()
        val check = generateCheckCharacter(prefixIssuingIdentityAndIdentifierToUpperCase)
        return "$prefixIssuingIdentityAndIdentifierToUpperCase#$check"
    }

    // see https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm
    private fun generateCheckCharacter(input: String): Char {
        var factor = 2
        var sum = 0
        val n = CODE_POINTS.length

        // Starting from the right and working leftwards is easier since
        // the initial "factor" will always be "2".
        for (i in input.length - 1 downTo 0) {
            val codePoint = codePointFromCharacter(input[i])
            var addend = factor * codePoint

            // Alternate the "factor" that each "codePoint" is multiplied by
            factor = if ((factor == 2)) 1 else 2

            // Sum the digits of the "addend" as expressed in base "n"
            addend = (addend / n) + (addend % n)
            sum += addend
        }

        // Calculate the number that must be added to the "sum"
        // to make it divisible by "n".
        val remainder = sum % n
        val checkCodePoint = (n - remainder) % n
        return CODE_POINTS[checkCodePoint]
    }

    private fun codePointFromCharacter(charAt: Char) =
        CODE_POINTS.indexOf(charAt)
            .apply {
                if (this < 0) {
                    throw IllegalStateException("Unsuppported character for checksum: $charAt")
                }
            }
}
