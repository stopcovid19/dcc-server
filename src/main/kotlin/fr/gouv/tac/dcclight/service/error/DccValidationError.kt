package fr.gouv.tac.dcclight.service.error

import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.aGGREGATECERTIFICATENOTALLOWED
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.bLOCKLISTEDCERTIFICATE
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.cERTIFICATEPROOFDATETOOFARINFUTURE
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.cWTEXPIRATIONTIMEEXPIRED
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.dCCDECRYPTIONERROR
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.fLEXIDMISMATCH
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.iNCOMPATIBLEDCCCOMBINATION
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.iNCONSISTENTFAMILYNAME
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.iNCONSISTENTGIVENNAME
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.iNVALIDDATEOFBIRTH
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.iNVALIDPUBLICKEY
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.mAXNUMBEROFCERTIFICATESEXCEEDED
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.mISSINGCERTIFICATEIDENTIFIER
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.mISSINGCERTIFICATEISSUER
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.mISSINGFAMILYNAME
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.mISSINGGIVENNAME
import fr.gouv.tac.dcclight.api.model.AggregateErrorResponseAllOfErrors.Code.mISSINGGIVENNAMETRANSLITERATED
import fr.gouv.tac.dcclight.service.error.DccValidationError.INVALID_PUBLIC_KEY
import org.springframework.validation.BindingResult

/**
 * Use this method to reject request "globally". It is currently used for technical errors that can happen during DCCs decoding.
 */
fun BindingResult.reject(error: DccValidationError) {
    this.reject(error.code.value, error.message)
}

/**
 * Use this method to reject the publicKey request attribute. It is currently used for technical errors linked to the
 * public key provided by the user.
 */
fun BindingResult.rejectPublicKey() {
    this.rejectValue("publicKey", INVALID_PUBLIC_KEY.code.value, INVALID_PUBLIC_KEY.message)
}

/**
 * Use this method to reject the certificates request attribute. It is currently used for functional errors such as
 * incompatible DCCs combination.
 */
fun BindingResult.rejectCertificates(error: DccValidationError) {
    this.rejectValue("certificates", error.code.value, error.message)
}

/**
 * Use this method to reject the certificates request object. It is currently used for functional errors linked to a
 * specific DCC, for example a missing field or an invalid value field.
 */
fun BindingResult.rejectCertificate(index: Int, error: DccValidationError) {
    this.rejectValue("certificates[$index]", error.code.value, error.message)
}

enum class DccValidationError(val code: Code, val message: String) {
    INCONSISTENT_FAMILY_NAME(
        iNCONSISTENTFAMILYNAME,
        "Family name does not match reference certificate family name"
    ),
    MISSING_FAMILY_NAME(
        mISSINGFAMILYNAME,
        "Missing family name in one of the provided dcc list"
    ),
    INCONSISTENT_GIVEN_NAME(
        iNCONSISTENTGIVENNAME,
        "Given name does not match reference certificate given name"
    ),
    INCOMPATIBLE_DCC_COMBINATION(
        iNCOMPATIBLEDCCCOMBINATION,
        "Provided certificates are not compatible with any operation"
    ),
    MISSING_GIVEN_NAME_TRANSLITERATED(
        mISSINGGIVENNAMETRANSLITERATED,
        "Missing transliterated given name"
    ),
    MISSING_GIVEN_NAME(
        mISSINGGIVENNAME,
        "Missing transliterated given name"
    ),
    MISSING_CERTIFICATE_ISSUER(
        mISSINGCERTIFICATEISSUER,
        "Missing certificate issuer"
    ),
    MISSING_CERTIFICATE_IDENTIFIER(
        mISSINGCERTIFICATEIDENTIFIER,
        "Missing certificate identifier"
    ),
    AGGREGATE_CERTIFICATE_NOT_ALLOWED(
        aGGREGATECERTIFICATENOTALLOWED,
        "Certificate comes from a previous aggregation"
    ),
    BLOCKLISTED_CERTIFICATE(
        bLOCKLISTEDCERTIFICATE,
        "Certificate is blocklisted"
    ),
    CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE(
        cERTIFICATEPROOFDATETOOFARINFUTURE,
        "Certificate proof date is in more than 24 hours"
    ),
    INVALID_PUBLIC_KEY(
        iNVALIDPUBLICKEY,
        "Request publicKey attribute is invalid"
    ),
    INVALID_DATE_OF_BIRTH(
        iNVALIDDATEOFBIRTH,
        "Invalid date of birth"
    ),
    FLEX_ID_MISMATCH(
        fLEXIDMISMATCH,
        "Flex id condition did not match"
    ),
    DCC_DECRYPTION_ERROR(
        dCCDECRYPTIONERROR,
        "Error during DCC decryption"
    ),
    CWT_EXPIRATION_TIME_EXPIRED(
        cWTEXPIRATIONTIMEEXPIRED,
        "CWT expirationTime field is in the past"
    ),
    MAX_NUMBER_OF_CERTIFICATES_EXCEEDED(
        mAXNUMBEROFCERTIFICATESEXCEEDED,
        "max number of certificates exceeded"
    )
}
