package fr.gouv.tac.dcclight.service.dcc.chain.dcclight

import com.upokecenter.cbor.CBORObject
import ehn.techiop.hcert.kotlin.crypto.CwtHeaderKeys
import kotlinx.datetime.Instant
import java.io.ByteArrayOutputStream
import kotlin.time.Duration

class DccLightCwtEUService(private val countryCode: String = "FR") {

    fun encode(input: ByteArray, issueTime: Instant, validity: Duration): ByteArray {
        val expirationTime = issueTime + validity
        val byteArrayOutputStream = ByteArrayOutputStream(16)
        return byteArrayOutputStream.also {
            CBORObject.WriteValue(it, 5, 4)
            CBORObject.Write(CwtHeaderKeys.ISSUER.intVal, it)
            CBORObject.Write(countryCode, it)
            CBORObject.Write(CwtHeaderKeys.ISSUED_AT.intVal, it)
            CBORObject.Write(issueTime.epochSeconds, it)
            CBORObject.Write(CwtHeaderKeys.EXPIRATION.intVal, it)
            CBORObject.Write(expirationTime.epochSeconds, it)
            CBORObject.Write(CwtHeaderKeys.HCERT.intVal, it)
            CBORObject.WriteValue(it, 5, 1)
            CBORObject.Write(CwtHeaderKeys.EUDGC_IN_HCERT.intVal, it)
            it.write(input)
        }.toByteArray()
    }
}
