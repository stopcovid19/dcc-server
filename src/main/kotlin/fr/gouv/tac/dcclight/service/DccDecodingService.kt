package fr.gouv.tac.dcclight.service

import ehn.techiop.hcert.kotlin.chain.CertificateRepository
import ehn.techiop.hcert.kotlin.chain.Chain
import ehn.techiop.hcert.kotlin.chain.ChainDecodeResult
import ehn.techiop.hcert.kotlin.chain.DecodeResult
import ehn.techiop.hcert.kotlin.chain.Error.CWT_EXPIRED
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCborService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCompressorService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultContextIdentifierService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCoseService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultCwtService
import ehn.techiop.hcert.kotlin.chain.impl.DefaultHigherOrderValidationService
import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.service.dcc.chain.OptimizedBase45EUService
import fr.gouv.tac.dcclight.service.dcc.chain.OptimizedSchemaValidationEUService
import fr.gouv.tac.dcclight.service.error.DccValidationError.CWT_EXPIRATION_TIME_EXPIRED
import fr.gouv.tac.dcclight.service.error.DccValidationError.DCC_DECRYPTION_ERROR
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import fr.gouv.tac.dcclight.validation.DccBlocklistValidator
import fr.gouv.tac.dcclight.validation.GreenCertificatesValidator
import org.springframework.stereotype.Service
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult

@Service
class DccDecodingService(
    repository: CertificateRepository,
    private val validators: List<GreenCertificatesValidator>,
    private val metricsService: MetricsService,
    private val blocklistValidator: DccBlocklistValidator
) {

    private val decodingChain = Chain(
        DefaultHigherOrderValidationService(),
        OptimizedSchemaValidationEUService(),
        DefaultCborService(),
        DefaultCwtService("FR"),
        DefaultCoseService(repository),
        DefaultCompressorService(),
        OptimizedBase45EUService(),
        DefaultContextIdentifierService()
    )

    fun decodeAndValidate(
        key: ByteArray,
        decryptedCertificates: List<String>,
        validation: BindingResult
    ): List<GreenCertificate> {

        val decodedCertificates = decryptedCertificates.map(decodingChain::decode)
        validateDecoding(decodedCertificates, validation)
        validateContent(decodedCertificates, validation)
        return decodedCertificates
            .map(DecodeResult::chainDecodeResult)
            .mapNotNull(ChainDecodeResult::eudgc)
    }

    private fun validateDecoding(decodedCertificates: List<DecodeResult>, validation: BindingResult) {
        decodedCertificates
            .onEachIndexed { index, decodedCertificate ->
                val error = decodedCertificate.verificationResult.error
                if (error != null) {
                    metricsService.incrementCounter(error!!)
                    metricsService.incrementCounter(DCC_DECRYPTION_ERROR)
                    if (error == CWT_EXPIRED) {
                        metricsService.logTechnicalValidityDateInThePast(decodedCertificate)
                        validation.rejectCertificate(index, CWT_EXPIRATION_TIME_EXPIRED)
                    } else {
                        validation.rejectCertificate(index, DCC_DECRYPTION_ERROR)
                    }
                }
            }
        if (validation.hasErrors()) throw BindException(validation)
    }

    private fun validateContent(decodeResults: List<DecodeResult>, validation: BindingResult) {
        if (decodeResults.any { it.chainDecodeResult.eudgc == null }) {
            throw IllegalStateException("DecodeResults eudgc fields are not supposed to be empty")
        }

        blocklistValidator.validate(
            decodeResults.map { it.chainDecodeResult },
            metricsService,
            validation
        )

        validators.forEach { validator ->
            validator.validate(
                decodeResults.map { it.chainDecodeResult.eudgc!! },
                metricsService,
                validation
            )
        }

        if (validation.hasErrors()) throw BindException(validation)
    }
}
