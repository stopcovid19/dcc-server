package fr.gouv.tac.dcclight.service.dcc.chain.dcclight

import com.upokecenter.cbor.CBORObject
import ehn.techiop.hcert.kotlin.chain.CertificateRepository
import ehn.techiop.hcert.kotlin.chain.CoseService
import ehn.techiop.hcert.kotlin.chain.CryptoService
import ehn.techiop.hcert.kotlin.chain.Error
import ehn.techiop.hcert.kotlin.chain.VerificationException
import ehn.techiop.hcert.kotlin.chain.VerificationResult
import ehn.techiop.hcert.kotlin.crypto.CoseHeaderKeys
import ehn.techiop.hcert.kotlin.crypto.CwtAlgorithm

const val SIGN1_TAG = 18

/**
 * Encodes/decodes input as a Sign1Message according to COSE specification (RFC8152)
 */
class DCCLightCoseEUService(
    private val cryptoService: CryptoService?,
    private val dcclightSigningService: DccLightSigningService?,
    private val verificationRepo: CertificateRepository? = null
) : CoseService {

    private fun addAttribute(objMap: CBORObject, key: CoseHeaderKeys, value: Any) {
        val content = if (value is CwtAlgorithm) value.intVal else value
        objMap.Add(CBORObject.FromObject(key.intVal), CBORObject.FromObject(content))
    }

    override fun encode(input: ByteArray): ByteArray {
        if (dcclightSigningService == null || cryptoService == null) throw NotImplementedError()

        val objProtected = CBORObject.NewMap()
        cryptoService.getCborHeaders().forEach {
            addAttribute(objProtected, it.first, it.second)
        }
        val bytesProtected = objProtected.EncodeToBytes()

        val objUnprotected = CBORObject.NewMap()

        val contextString = "Signature1"
        val externalData = ByteArray(0)

        val objToSign = CBORObject.NewArray()
        objToSign.Add(contextString)
        objToSign.Add(bytesProtected)
        objToSign.Add(externalData)
        objToSign.Add(input)

        val signature = dcclightSigningService.sign(objToSign.EncodeToBytes())

        val obj = CBORObject.NewArray()
        obj.Add(bytesProtected)
        obj.Add(objUnprotected)
        obj.Add(input)
        obj.Add(signature)

        return CBORObject.FromObjectAndTag(obj, SIGN1_TAG).EncodeToBytes()
    }

    override fun decode(input: ByteArray, verificationResult: VerificationResult): ByteArray {
        val coseAdapter = DccLightCoseAdapter(input)
        val kid = coseAdapter.getProtectedAttributeByteArray(CoseHeaderKeys.KID.intVal)
            ?: coseAdapter.getUnprotectedAttributeByteArray(CoseHeaderKeys.KID.intVal)
            ?: throw VerificationException(Error.KEY_NOT_IN_TRUST_LIST, "KID not found")
        // TODO is the algorithm relevant?
        // val algorithm = coseAdapter.getProtectedAttributeInt(CoseHeaderKeys.Algorithm.value)
        if (verificationRepo != null) {
            if (!coseAdapter.validate(kid, verificationRepo, verificationResult))
                throw VerificationException(Error.SIGNATURE_INVALID, "Not validated")
        } else if (cryptoService != null) {
            if (!coseAdapter.validate(kid, cryptoService, verificationResult))
                throw VerificationException(Error.SIGNATURE_INVALID, "Not validated")
        } else {
            // safe to throw this "ugly" error, as it should not happen
            throw NotImplementedError()
        }
        return coseAdapter.getContent()
    }
}
