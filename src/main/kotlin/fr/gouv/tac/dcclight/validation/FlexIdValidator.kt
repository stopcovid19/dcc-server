package fr.gouv.tac.dcclight.validation

import fr.gouv.tac.dcclight.extension.removeAccents

abstract class FlexIdValidator {

    internal fun matchFlexId(names: List<String>): Boolean {
        val smallestNamesList = names
            .map { it.split('<') }
            .map(List<String>::toSet)
            .minByOrNull { it.size }!!
        return names
            .map { it.split('<') }
            .all { certificateNamesList -> certificateNamesList.containsAll(smallestNamesList) }
    }

    internal fun matchesFlexIdAfterRemovingAccents(names: List<String>) = matchFlexId(names.map { it.removeAccents() })
}
