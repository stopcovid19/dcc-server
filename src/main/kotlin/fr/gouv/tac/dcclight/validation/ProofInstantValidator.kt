package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getProofInstant
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.error.DccValidationError.CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import kotlinx.datetime.Clock
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult
import kotlin.time.Duration.Companion.days

@Component
class ProofInstantValidator : GreenCertificatesValidator {

    override fun validate(
        /*
        Should logically take only LocalDate attribute values, but because we have to log information on the error DCC
        we have to take the whole certificates as input
         */
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult
    ) {
        greenCertificatesToValidate.forEachIndexed { index, dcc ->
            if (dcc.getProofInstant() > Clock.System.now().plus(1.days)) {
                validation.rejectCertificate(index, CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE)
                metricsService.incrementCounter(CERTIFICATE_PROOF_DATE_TOO_FAR_IN_FUTURE)
                metricsService.logRejectDccWithDateInFutureMoreThanOneDay(dcc)
            }
        }
    }
}
