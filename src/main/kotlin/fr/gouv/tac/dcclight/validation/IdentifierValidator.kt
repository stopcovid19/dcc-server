package fr.gouv.tac.dcclight.validation

import ehn.techiop.hcert.kotlin.data.GreenCertificate
import fr.gouv.tac.dcclight.extension.getIdentifier
import fr.gouv.tac.dcclight.service.MetricsService
import fr.gouv.tac.dcclight.service.error.DccValidationError.AGGREGATE_CERTIFICATE_NOT_ALLOWED
import fr.gouv.tac.dcclight.service.error.DccValidationError.MISSING_CERTIFICATE_IDENTIFIER
import fr.gouv.tac.dcclight.service.error.rejectCertificate
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult

@Component
class IdentifierValidator : GreenCertificatesValidator {
    override fun validate(
        greenCertificatesToValidate: List<GreenCertificate>,
        metricsService: MetricsService,
        validation: BindingResult,
    ) {
        greenCertificatesToValidate.forEachIndexed { index, dcc ->
            if (dcc.getIdentifier().isEmpty()) {
                validation.rejectCertificate(index, MISSING_CERTIFICATE_IDENTIFIER)
                metricsService.incrementCounter(MISSING_CERTIFICATE_IDENTIFIER)
            }
            if (dcc.getIdentifier().startsWith("URN:UVCI:01:FR:DGSAG/")) {
                validation.rejectCertificate(index, AGGREGATE_CERTIFICATE_NOT_ALLOWED)
                metricsService.incrementCounter(AGGREGATE_CERTIFICATE_NOT_ALLOWED)
            }
        }
    }
}
